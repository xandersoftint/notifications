# README #

This README will help you setup your environment for the order text messages microservice.

## Microservice Functionality Description ##
Provides the ability to create a restaurant with multiple delivery intervals, in which depending on the order hour, a restaurant could have fluctuating delivery estimations. Also, you can configure a default delivery estimation through the api, that will be taken in consideration when the order completion time does not match any existing hour intervals.
Exposes CRUD contracts for specific entities, but also Action contracts that are able to sync and async send notifications (only MessageBird sms notifications are supported).  
Async notification sending functionality was done by using a queue system, in order to increase stability, performance, scalability of consumer responses.
By using the queue system, we can easily have delayed messages sent.

Please consult the __openapi__ documentation for exposed API contracts. 

### Important Project Folder Structure ###
* ./docker/ - holds the Docker recipes in order to setup the infrastructure;
* ./src/ - holds the "App" namespace source code;
* ./tests/ - holds the "App" namespace unit tests;
* ./http/ - holds the available APIs with examples for manual functional testing of the exposed contracts
* ./openapi/ - holds the openapi v3 version of the existing REST API contracts, which could be easily imported in a swagger environment  
_Once you made your docker containers up and running you can directly access the openapi dump from your browser  [here](http://notifications.takeaway.local/openapi/v1/). Copy and paste the generated json to [swagger](https://editor.swagger.io) to see all the available microservice API contracts._

### Important Project Technologies, Frameworks, Integrators ###
* docker for infrastructure setup
* symfony 5.x framework for source code implementation
* phpunit for unit testing
* nginx for routing
* php-fpm with php version 7.4.3 for code execution
* mysql 8 for storage
* rabbit-mq for queued async calls to external integrators
* MessageBird as the external SMS Integrator
* Twig, Bootstrap and jQuery for templating

### How do I get set up? ###

* __Infrastructure setup__
  * advance to the docker folder of the project
  * generate an .env file from the .env.dist
  * update the __WORKSPACES_APP_PATH__ with the parent absolute path of the project which is mapped on your shared docker machine. 
    In the example below you'll need to add the path to the __takeaway__ folder  
  ```shell script  
    mkdir takeway && cd takeway && git clone git@bitbucket.org:xandersoftint/notifications.git && pwd
  ```
  * run "docker-compose up -d" command from the docker folder to setup your infrastructure
  * nginx container is configured to vhost to "http://notifications.takeaway.local", therefore you will need to map the domain to your "/etc/hosts" file.  
  
* __Configuration__  
In the project directory you may find an .env.dist file which holds the required configuration in order for the framework to run.
  * Run composer install command to get vendor packages  
  * Run the following command and configure your .env file accordingly:  
  ```shell script
      cp ./env.dist ./env
  ```
  * a "test" key for MessageBird is stored in the .env.dist file.
  * setup the rabbit mq fabric by running  
  ```shell script
     php bin/console messenger:setup
  ```
  * start the notification queue consumer by running the following command in the php-fpm container. You can send the execution to the background by adding & at the end of the command, but you will not detect when it consumer crashes easily :).  
  ```shell script
     php bin/console messenger:consume notifications
  ```  
  
* __Database configuration__  
  Create the "notifications" schema  
  
  ```mysql
    CREATE SCHEMA notifications;
  ```
  
  Run the following command on the php-fpm docker-container to execute schema and seeding migrations
    
  ```shell script
    php bin/console doctrine:migrations:migrate
  ```  
  
* __How to run tests__  
  In order to run the tests advance to the project directory and run the following command  
  ```shell script
    ./vendor/bin/simple-phpunit #will install on demand and one time phpunit and run the tests
  ```

### Admin ###
The admin section can be accessed at the "http://notifications.takeaway.local/admin/".  
A list of transactions is populated by using paginated ajax calls to the "priority" api.  

### Side Notes ###
This notification microservice combines the business logic of 2 domains, more exactly the restaurant and the notification domains.  
We therefore assume that the caller knows and sends the restaurant identification code when consuming it's contracts, therefore in this case, both microservices will know the Restaurant domain.  
This could have been avoided if:  

* each domain should have had it's own microservice (Restaurant + Notifications);
* Order (microservice) already having context of the restaurant in order to store references of the finalised order should have therefore consumed the Restaurant contracts to retrieve additional information needed to build a message.
* Order afterwards should have been composed a message with context of the restaurant and consumed Notifications contracts to send it.
* Notifications should only have been specialised in sending different types of notifications (emails, push, sms, etc) having only context of notification external integrators, adaptation to them and of course templating and hydration for message bodies.

Having the above approach you could have had specialised microservices without having unnecessary context duplicated between them.

### TODOs ###
* seeding service with versioning functionality in order to avoid using migrations as seeders
* config service to be able to create environment configuration automatically not via code
* add caching functionality - redis
* add multiple types of notifications - email, push
* sending retrials to integrators on notification errors

### Who do I talk to? ###
* Ionut-Alexandru Banica - alexandru.banica@xandersoftint.com / ionut.alexandru.banica@gmail.com