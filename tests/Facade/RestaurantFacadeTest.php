<?php
declare(strict_types=1);

namespace App\Tests\Facade;

use App\Dto\DtoInterface;
use App\Dto\Restaurant as RestaurantDto;
use App\Entity\DeliveryInterval;
use App\Entity\EntityInterface;
use App\Entity\Restaurant;
use App\Facade\RestaurantFacade;
use App\Hydrator\DtoHydrator;
use App\Hydrator\EntityHydrator;
use App\Repository\RestaurantRepository;
use App\Service\DeliveryIntervalService;
use App\Service\ObjectHydratorService;
use App\Service\RestaurantService;
use App\Service\TypeConverterService;
use PHPUnit\Framework\TestCase;
use Symfony\Component\Serializer\NameConverter\NameConverterInterface;
use Symfony\Component\Serializer\SerializerInterface;

class RestaurantFacadeTest extends TestCase
{
    private ObjectHydratorService $hydrator;
    private SerializerInterface $serializer;

    protected function setUp(): void
    {
        $this->hydrator = new ObjectHydratorService();
        $this->hydrator->addHydrator(EntityInterface::class, new EntityHydrator());
        $this->hydrator->addHydrator(DtoInterface::class, new DtoHydrator());

        $this->serializer = $this->getMockBuilder(SerializerInterface::class)->setMethods([])->getMock();

        parent::setUp();
    }

    public function testGenerateADtoFromRawInput(): RestaurantDto
    {
        $restaurantServiceStub = $this->getMockBuilder(RestaurantService::class)
            ->disableOriginalConstructor()
            ->setMethods()
            ->getMock();

        $deliveryIntervalServiceStub = $this->getMockBuilder(DeliveryIntervalService::class)
            ->disableOriginalConstructor()
            ->setMethods([])
            ->getMock();

        $nameConverterStub = $this->getMockBuilder(NameConverterInterface::class)->setMethods([])->getMock();

        $facade = new RestaurantFacade(
            $restaurantServiceStub,
            $deliveryIntervalServiceStub
        );

        $facade->setConverter(
            new TypeConverterService(
                $this->serializer,
                $this->hydrator,
                $nameConverterStub
            )
        );

        $result = $facade->generate([
            'code' => 'restaurant_code',
            'name' => 'Restaurant Name',
            'intervals' => [
                [
                    'beginAt' => '2012-12-12 12:00:00',
                    'endAt' => '2012-12-12 13:00:00',
                    'estimation' => '30M',
                ],
                [
                    'beginAt' => '2012-12-12 14:00:00',
                    'endAt' => '2012-12-12 15:00:00',
                    'estimation' => '15M',
                ],
            ],
        ]);

        $this->assertEquals('restaurant_code', $result->code);
        $this->assertEquals('Restaurant Name', $result->name);
        $this->assertCount(2, $result->intervals);
        $this->assertEquals(30, $result->intervals[0]->estimation->i);
        $this->assertEquals(15, $result->intervals[1]->estimation->i);

        return $result;
    }


    /** @depends testGenerateADtoFromRawInput */
    public function testCreateFunctionalityHavingAllPropertiesDtoCreatesObject(RestaurantDto $restaurantDto): void
    {
        $restaurantServiceMock = $this->getMockBuilder(RestaurantService::class)
            ->disableOriginalConstructor()
            ->setMethods(['create'])
            ->getMock();
        $deliveryIntervalServiceMock = $this->getMockBuilder(DeliveryIntervalService::class)
            ->disableOriginalConstructor()
            ->setMethods(['create'])
            ->getMock();
        $restaurantServiceMock->setRepository(
            $this->getMockBuilder(RestaurantRepository::class)
                ->disableOriginalConstructor()
                ->setMethods([])
                ->getMock()
        );

        $restaurantServiceMock->expects($this->once())->method('create')->with([
            'code' => 'restaurant_code',
            'name' => 'Restaurant Name',
        ])->willReturn((new Restaurant())->setCode('expected_result_code'));

        $deliveryIntervalServiceMock->expects($this->exactly(2))->method('create');
        $nameConverterStub = $this->getMockBuilder(NameConverterInterface::class)->setMethods([])->getMock();

        $facade = new RestaurantFacade(
            $restaurantServiceMock,
            $deliveryIntervalServiceMock,
            new TypeConverterService(
                $this->serializer,
                $this->hydrator,
                $nameConverterStub
            )
        );

        $result = $facade->create($restaurantDto);
        $this->assertEquals('expected_result_code', $result->getCode());
    }

    public function testGetEstimationForCurrentTime()
    {
        $restaurantServiceStub = $this->getMockBuilder(RestaurantService::class)
            ->disableOriginalConstructor()
            ->setMethods([])
            ->getMock();
        $deliveryIntervalServiceMock = $this->getMockBuilder(DeliveryIntervalService::class)
            ->disableOriginalConstructor()
            ->setMethods(['getByRestaurantAndTime'])
            ->getMock();

        $restaurantServiceStub->setRepository(
            $this->getMockBuilder(RestaurantRepository::class)
                ->disableOriginalConstructor()
                ->setMethods([])
                ->getMock()
        );

        $nameConverterStub = $this->getMockBuilder(NameConverterInterface::class)->setMethods([])->getMock();

        $facade = new RestaurantFacade(
            $restaurantServiceStub,
            $deliveryIntervalServiceMock,
            new TypeConverterService(
                $this->serializer,
                $this->hydrator,
                $nameConverterStub
            )
        );

        $deliveryIntervalServiceMock
            ->expects($this->once())
            ->method('getByRestaurantAndTime')
            ->with(new Restaurant())
            ->willReturn((new DeliveryInterval())->setEstimation(new \DateInterval('PT30M')));

        $this->assertEquals(30, $facade->getEstimationForCurrentTime(new Restaurant())->i);
    }

    protected function tearDown(): void
    {
        unset($this->serializer, $this->hydrator);
        parent::tearDown();
    }
}
