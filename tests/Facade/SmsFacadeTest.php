<?php
declare(strict_types=1);

namespace App\Tests\Facade;

use App\Dto\DtoInterface;
use App\Dto\Sms as SmsDto;
use App\Entity\EntityInterface;
use App\Entity\NotificationInterface;
use App\Entity\Sms;
use App\Exception\IntegrationRespondedException;
use App\Facade\SmsFacade;
use App\Hydrator\DtoHydrator;
use App\Hydrator\EntityHydrator;
use App\Integration\NotificationInterface as IntegrationNotificationInterface;
use App\Message\SmsMessage;
use App\MessageProducer\NotificationProducer;
use App\Repository\SmsRepository;
use App\Service\ObjectHydratorService;
use App\Service\SmsService;
use App\Service\SmsTransactionService;
use App\Service\TypeConverterService;
use PHPUnit\Framework\TestCase;
use Symfony\Component\Serializer\NameConverter\NameConverterInterface;
use Symfony\Component\Serializer\SerializerInterface;
use App\Dto\Integration\Response as IntegrationResponse;

class SmsFacadeTest extends TestCase
{
    private ObjectHydratorService $hydrator;
    private SerializerInterface $serializer;

    protected function setUp(): void
    {
        $this->hydrator = new ObjectHydratorService();
        $this->hydrator->addHydrator(EntityInterface::class, new EntityHydrator());
        $this->hydrator->addHydrator(DtoInterface::class, new DtoHydrator());

        $this->serializer = $this
            ->getMockBuilder(SerializerInterface::class)
            ->setMethods(['serialize', 'deserialize'])
            ->getMock();

        parent::setUp();
    }

    public function testGenerateADtoFromRawInput(): SmsDto
    {
        $smsServiceStub = $this->getMockBuilder(SmsService::class)
            ->disableOriginalConstructor()
            ->setMethods([])
            ->getMock();
        $smsTransactionServiceStub = $this->getMockBuilder(SmsTransactionService::class)
            ->disableOriginalConstructor()
            ->setMethods([])
            ->getMock();
        $nameConverterStub = $this->getMockBuilder(NameConverterInterface::class)->setMethods([])->getMock();
        $integrationStub = $this->getMockBuilder(IntegrationNotificationInterface::class)->setMethods([])->getMock();
        $notificationProducer = $this->getMockBuilder(NotificationProducer::class)
            ->disableOriginalConstructor()
            ->setMethods([])
            ->getMock();

        $facade = new SmsFacade(
            $smsServiceStub,
            $smsTransactionServiceStub,
            $integrationStub,
            $notificationProducer
        );
        $facade->setConverter(
            new TypeConverterService(
                $this->serializer,
                $this->hydrator,
                $nameConverterStub
            )
        );

        /** @var SmsDto $result */
        $result = $facade->generate([
                'from' => 'Hard Rock',
                'destination' => '+40722222222',
                'message' => 'Hard Rock Cafe will deliver your meal in 30 minutes'
        ]);

        $this->assertEquals('+40722222222', $result->destination);
        $this->assertEquals('Hard Rock', $result->from);
        $this->assertEquals('Hard Rock Cafe will deliver your meal in 30 minutes', $result->message);

        return $result;
    }

    public function testGenerateADtoFromRawInputWithDefaultFromValueIfExceedsCharacterLength(): void
    {
        $smsServiceStub = $this->getMockBuilder(SmsService::class)
            ->disableOriginalConstructor()
            ->setMethods([])
            ->getMock();
        $smsTransactionServiceStub = $this->getMockBuilder(SmsTransactionService::class)
            ->disableOriginalConstructor()
            ->setMethods([])
            ->getMock();
        $nameConverterStub = $this->getMockBuilder(NameConverterInterface::class)->setMethods([])->getMock();
        $integrationStub = $this->getMockBuilder(IntegrationNotificationInterface::class)->setMethods([])->getMock();
        $notificationProducer = $this->getMockBuilder(NotificationProducer::class)
            ->disableOriginalConstructor()
            ->setMethods([])
            ->getMock();

        $facade = new SmsFacade(
            $smsServiceStub,
            $smsTransactionServiceStub,
            $integrationStub,
            $notificationProducer
        );
        $facade->setConverter(
            new TypeConverterService(
                $this->serializer,
                $this->hydrator,
                $nameConverterStub
            )
        );

        /** @var SmsDto $result */
        $result = $facade->generate([
            'from' => 'Hard Rock Cafe',
            'destination' => '+40722222222',
            'message' => 'Hard Rock Cafe will deliver your meal in 30 minutes'
        ]);

        $this->assertEquals('+40722222222', $result->destination);
        $this->assertEquals('Hard Rock Cafe', $result->from);
        $this->assertEquals('Hard Rock Cafe will deliver your meal in 30 minutes', $result->message);
        $this->assertEquals([
            'from' => 'Takeaway',
            'destination' => '+40722222222',
            'message' => 'Hard Rock Cafe will deliver your meal in 30 minutes'
        ], $result->toArray());
    }


    /** @depends testGenerateADtoFromRawInput */
    public function testCreateFunctionalityHavingAllPropertiesDtoCreatesObject(SmsDto $smsDto): Sms
    {
        $smsServiceMock = $this->getMockBuilder(SmsService::class)
            ->disableOriginalConstructor()
            ->setMethods(['create'])
            ->getMock();

        $smsServiceMock->expects($this->once())->method('create')->with([
            'from' => 'Hard Rock',
            'destination' => '+40722222222',
            'message' => 'Hard Rock Cafe will deliver your meal in 30 minutes'
        ])->willReturn(
            (new Sms())
                ->setFrom('Hard Rock')
                ->setDestination('+40722222222')
                ->setMessage('Hard Rock Cafe will deliver your meal in 30 minutes')
        );

        $smsTransactionServiceStub = $this->getMockBuilder(SmsTransactionService::class)
            ->disableOriginalConstructor()
            ->setMethods([])
            ->getMock();

        $nameConverterStub = $this->getMockBuilder(NameConverterInterface::class)->setMethods([])->getMock();
        $integrationStub = $this->getMockBuilder(IntegrationNotificationInterface::class)->setMethods([])->getMock();
        $notificationProducer = $this->getMockBuilder(NotificationProducer::class)
            ->disableOriginalConstructor()
            ->setMethods([])
            ->getMock();

        $facade = new SmsFacade(
            $smsServiceMock,
            $smsTransactionServiceStub,
            $integrationStub,
            $notificationProducer
        );

        $facade->setConverter(
            new TypeConverterService(
                $this->serializer,
                $this->hydrator,
                $nameConverterStub
            ),
        );

        $result = $facade->create($smsDto);
        $this->assertEquals('+40722222222', $result->getDestination());
        $this->assertEquals('Hard Rock Cafe will deliver your meal in 30 minutes', $result->getMessage());
        $this->assertEquals('new', $result->getStatus());
        $this->assertEquals('Hard Rock', $result->getFrom());

        return $result;
    }

    /** @depends testCreateFunctionalityHavingAllPropertiesDtoCreatesObject */
    public function testSendOnSuccess(Sms $sms)
    {
        $smsServiceMock = $this->getMockBuilder(SmsService::class)
            ->disableOriginalConstructor()
            ->setMethods(['update'])
            ->getMock();

        $smsTransactionServiceMock = $this->getMockBuilder(SmsTransactionService::class)
            ->disableOriginalConstructor()
            ->setMethods(['create'])
            ->getMock();
        $nameConverterStub = $this->getMockBuilder(NameConverterInterface::class)->setMethods([])->getMock();

        $integrationMock = $this->getMockBuilder(IntegrationNotificationInterface::class)
            ->setMethods(['send'])
            ->getMock();

        $integrationMock->expects($this->once())->method('send')->with(
            ['+40722222222'],
            'Hard Rock',
            'Hard Rock Cafe will deliver your meal in 30 minutes'
        )->willReturn((new IntegrationResponse()));

        $repositoryStub = $this->getMockBuilder(SmsRepository::class)
            ->disableOriginalConstructor()
            ->setMethods([])
            ->getMock();
        $smsServiceMock->setRepository($repositoryStub);

        $smsServiceMock
            ->expects($this->once())
            ->method('update')
            ->with($sms, ['status' => NotificationInterface::STATUS_SENT])
            ->willReturn($sms->setStatus(NotificationInterface::STATUS_SENT));

        $smsTransactionServiceMock
            ->expects($this->once())
            ->method('create')
            ->with(
                [
                    'sms' => $sms,
                    'status' => 200,
                    'response' => []
                ]
            );

        $notificationProducer = $this->getMockBuilder(NotificationProducer::class)
            ->disableOriginalConstructor()
            ->setMethods([])
            ->getMock();

        $facade = new SmsFacade(
            $smsServiceMock,
            $smsTransactionServiceMock,
            $integrationMock,
            $notificationProducer
        );

        $facade->setConverter(
            new TypeConverterService(
                $this->serializer,
                $this->hydrator,
                $nameConverterStub
            )
        );

        $response = $facade->send($sms);
        $this->assertEquals('+40722222222', $response->getDestination());
        $this->assertEquals('Hard Rock', $response->getFrom());
        $this->assertEquals('Hard Rock Cafe will deliver your meal in 30 minutes', $response->getMessage());
        $this->assertEquals(NotificationInterface::STATUS_SENT, $response->getStatus());
    }

    /** @depends testCreateFunctionalityHavingAllPropertiesDtoCreatesObject */
    public function testSendOnFailure(Sms $sms)
    {
        $smsServiceMock = $this->getMockBuilder(SmsService::class)
            ->disableOriginalConstructor()
            ->setMethods(['update'])
            ->getMock();

        $smsTransactionServiceMock = $this->getMockBuilder(SmsTransactionService::class)
            ->disableOriginalConstructor()
            ->setMethods(['create'])
            ->getMock();

        $nameConverterStub = $this->getMockBuilder(NameConverterInterface::class)->setMethods([])->getMock();

        $integrationMock = $this->getMockBuilder(IntegrationNotificationInterface::class)
            ->setMethods(['send'])
            ->getMock();

        $integrationMock->expects($this->once())->method('send')->with(
            ['+40722222222'],
            'Hard Rock',
            'Hard Rock Cafe will deliver your meal in 30 minutes'
        )->willThrowException(
            (new IntegrationRespondedException('Purpose Bad Request', 400))->setResponse(new IntegrationResponse())
        );

        $repositoryStub = $this->getMockBuilder(SmsRepository::class)
            ->disableOriginalConstructor()
            ->setMethods([])
            ->getMock();
        $smsServiceMock->setRepository($repositoryStub);

        $smsServiceMock
            ->expects($this->once())
            ->method('update')
            ->with($sms, ['status' => NotificationInterface::STATUS_ERROR])
            ->willReturn($sms->setStatus(NotificationInterface::STATUS_ERROR));

        $smsTransactionServiceMock
            ->expects($this->once())
            ->method('create')
            ->with(
                [
                    'sms' => $sms,
                    'status' => 400,
                    'response' => []
                ]
            );

        $notificationProducer = $this->getMockBuilder(NotificationProducer::class)
            ->disableOriginalConstructor()
            ->setMethods([])
            ->getMock();

        $facade = new SmsFacade(
            $smsServiceMock,
            $smsTransactionServiceMock,
            $integrationMock,
            $notificationProducer
        );

        $facade->setConverter(
            new TypeConverterService(
                $this->serializer,
                $this->hydrator,
                $nameConverterStub
            )
        );

        $response = $facade->send($sms);
        $this->assertEquals('+40722222222', $response->getDestination());
        $this->assertEquals('Hard Rock', $response->getFrom());
        $this->assertEquals('Hard Rock Cafe will deliver your meal in 30 minutes', $response->getMessage());
        $this->assertEquals(NotificationInterface::STATUS_ERROR, $response->getStatus());
    }

    /** @depends testCreateFunctionalityHavingAllPropertiesDtoCreatesObject */
    public function testEnvelopeAndSend(Sms $sms)
    {
        $smsServiceMock = $this->getMockBuilder(SmsService::class)
            ->disableOriginalConstructor()
            ->setMethods(['create', 'update'])
            ->getMock();

        $smsTransactionServiceMock = $this->getMockBuilder(SmsTransactionService::class)
            ->disableOriginalConstructor()
            ->setMethods(['create'])
            ->getMock();

        $integrationMock = $this->getMockBuilder(IntegrationNotificationInterface::class)
            ->setMethods([])
            ->getMock();

        $repositoryStub = $this->getMockBuilder(SmsRepository::class)
            ->disableOriginalConstructor()
            ->setMethods([])
            ->getMock();
        $smsServiceMock->setRepository($repositoryStub);

        $smsServiceMock
            ->expects($this->exactly(2))
            ->method('create')
            ->willReturn($sms);

        $nameConverterStub = $this->getMockBuilder(NameConverterInterface::class)->setMethods([])->getMock();

        $notificationProducer = $this->getMockBuilder(NotificationProducer::class)
            ->disableOriginalConstructor()
            ->setMethods(['getMessageClassByTopic', 'publish', 'publishForDelayedProcessing'])
            ->getMock();

        $notificationProducer
            ->expects($this->exactly(2))
            ->method('getMessageClassByTopic')
            ->with(SmsMessage::TOPIC)
            ->willReturn(SmsMessage::class);

        $notificationProducer
            ->expects($this->exactly(2))
            ->method('publish')
            ->with(new SmsMessage(0));

        $notificationProducer
            ->expects($this->never())
            ->method('publishForDelayedProcessing');

        $facade = new SmsFacade(
            $smsServiceMock,
            $smsTransactionServiceMock,
            $integrationMock,
            $notificationProducer
        );

        $facade->setConverter(
            new TypeConverterService(
                $this->serializer,
                $this->hydrator,
                $nameConverterStub
            )
        );

        $facade->envelopeAndSend(
            'Hard Rock',
            ['+40722222222', '+40722222222'],
            'Hard Rock Cafe will deliver your meal in 30 minutes'
        );
    }

    /** @depends testCreateFunctionalityHavingAllPropertiesDtoCreatesObject */
    public function testEnvelopeAndSendDelayed(Sms $sms)
    {
        $smsServiceMock = $this->getMockBuilder(SmsService::class)
            ->disableOriginalConstructor()
            ->setMethods(['create', 'update'])
            ->getMock();

        $smsTransactionServiceMock = $this->getMockBuilder(SmsTransactionService::class)
            ->disableOriginalConstructor()
            ->setMethods(['create'])
            ->getMock();

        $integrationMock = $this->getMockBuilder(IntegrationNotificationInterface::class)
            ->setMethods([])
            ->getMock();

        $repositoryStub = $this->getMockBuilder(SmsRepository::class)
            ->disableOriginalConstructor()
            ->setMethods([])
            ->getMock();
        $smsServiceMock->setRepository($repositoryStub);

        $smsServiceMock
            ->expects($this->exactly(2))
            ->method('create')
            ->willReturn($sms);

        $nameConverterStub = $this->getMockBuilder(NameConverterInterface::class)->setMethods([])->getMock();

        $notificationProducer = $this->getMockBuilder(NotificationProducer::class)
            ->disableOriginalConstructor()
            ->setMethods(['getMessageClassByTopic', 'publish', 'publishForDelayedProcessing'])
            ->getMock();

        $notificationProducer
            ->expects($this->exactly(2))
            ->method('getMessageClassByTopic')
            ->with(SmsMessage::TOPIC)
            ->willReturn(SmsMessage::class);

        $notificationProducer
            ->expects($this->never())
            ->method('publish');

        $notificationProducer
            ->expects($this->exactly(2))
            ->method('publishForDelayedProcessing')
            ->with(new SmsMessage(0), 90 * 60);

        $facade = new SmsFacade(
            $smsServiceMock,
            $smsTransactionServiceMock,
            $integrationMock,
            $notificationProducer
        );

        $facade->setConverter(
            new TypeConverterService(
                $this->serializer,
                $this->hydrator,
                $nameConverterStub
            )
        );

        $facade->envelopeAndSend(
            'Hard Rock',
            ['+40722222222', '+40722222222'],
            'Hard Rock Cafe will deliver your meal in 30 minutes',
            90 * 60
        );
    }

    protected function tearDown(): void
    {
        unset($this->serializer, $this->hydrator);
        parent::tearDown();
    }
}
