<?php
declare(strict_types=1);

namespace App\Tests\Facade;

use App\Dto\DtoInterface;
use App\Dto\Notification\Order\ConfirmationTemplate as ConfirmationNotification;
use App\Dto\Notification\Order\ReviewTemplate as ReviewNotification;
use App\Entity\EntityInterface;
use App\Entity\Restaurant;
use App\Entity\Sms;
use App\Facade\NotificationFacadeInterface;
use App\Facade\OrderFacade;
use App\Facade\RestaurantFacade;
use App\Facade\SmsFacade;
use App\Hydrator\DtoHydrator;
use App\Hydrator\EntityHydrator;
use App\Service\EnvironmentService;
use App\Service\ObjectHydratorService;
use App\Service\TypeConverterService;
use PHPUnit\Framework\TestCase;
use Psr\Log\LoggerInterface;
use Symfony\Component\Serializer\NameConverter\CamelCaseToSnakeCaseNameConverter;
use Symfony\Component\Serializer\SerializerInterface;
use Symfony\Contracts\Translation\TranslatorInterface;

class OrderFacadeTest extends TestCase
{
    private NotificationFacadeInterface $notificationFacade;
    private RestaurantFacade $restaurantFacade;
    private OrderFacade $facade;

    protected function setUp(): void
    {
        $environment = new EnvironmentService('/', 'en');
        $environment->setConfig(5400);

        $loggerStub = $this->getMockBuilder(LoggerInterface::class)->setMethods([])->getMock();
        $translatorStub = $this->getMockBuilder(TranslatorInterface::class)->setMethods([])->getMock();
        $translatorStub->method('trans')->willReturnArgument(0);

        $this->restaurantFacade = $this->getMockBuilder(RestaurantFacade::class)
            ->disableOriginalConstructor()
            ->setMethods([])
            ->getMock();
        $this->notificationFacade = $this->getMockBuilder(SmsFacade::class)
            ->disableOriginalConstructor()
            ->setMethods([])
            ->getMock();

        $hydrator = new ObjectHydratorService();
        $hydrator->addHydrator(EntityInterface::class, new EntityHydrator());
        $hydrator->addHydrator(DtoInterface::class, new DtoHydrator());

        $serializer = $this->getMockBuilder(SerializerInterface::class)->setMethods([])->getMock();

        $this->facade = new OrderFacade(
            $this->restaurantFacade,
            $this->notificationFacade,
            $translatorStub,
            $environment
        );
        $this->facade->setConverter(
            new TypeConverterService(
                $serializer,
                $hydrator,
                new CamelCaseToSnakeCaseNameConverter()
            )
        );
        $this->facade->setLogger($loggerStub);

        parent::setUp();
    }

    public function testGenerateConfirmationNotification(): ConfirmationNotification
    {
        $expected = new ConfirmationNotification();
        $expected->from = 'hard_rock';
        $expected->destinations = ['+40722222222', '+40733333333'];

        $this->assertEquals($expected, $this->facade->generateConfirmationNotification([
            'from' => 'hard_rock',
            'destinations' => ['+40722222222', '+40733333333']
        ]));

        return $expected;
    }

    public function testGenerateReviewNotification(): void
    {
        $expected = new ReviewNotification();
        $expected->from = 'hard_rock';
        $expected->destinations = ['+40722222222', '+40733333333'];

        $this->assertEquals($expected, $this->facade->generateReviewNotification([
            'from' => 'hard_rock',
            'destinations' => ['+40722222222', '+40733333333']
        ]));
    }

    /** @depends testGenerateConfirmationNotification */
    public function testSendNotification(ConfirmationNotification $notification): void
    {
        $restaurant = (new Restaurant())->setCode('hard_code')->setName('Hard Rock Cafe');
        $estimation = new \DateInterval('PT1H30M');
        $this->restaurantFacade
            ->expects($this->once())
            ->method('__call')
            ->with('getByCode',['hard_rock'])
            ->willReturn($restaurant);

        $this->restaurantFacade
            ->expects($this->once())
            ->method('getEstimationForCurrentTime')
            ->with($restaurant)
            ->willReturn($estimation);

        $this->notificationFacade
            ->expects($this->once())
            ->method('envelopeAndSend')
            ->with(
                'Hard Rock Cafe',
                ['+40722222222', '+40733333333'],
                "Hard Rock Cafe will deliver your meal in 1 hours 30 minutes",
                0
            );

        $this->facade->sendNotification($notification);
    }

    /** @depends testGenerateConfirmationNotification */
    public function testSendNotificationWithDelayedTimeComputation(ConfirmationNotification $notification): void
    {
        $restaurant = (new Restaurant())->setCode('hard_code')->setName('Hard Rock Cafe');
        $estimation = new \DateInterval('PT1H30M');
        $this->restaurantFacade
            ->expects($this->once())
            ->method('__call')
            ->with('getByCode',['hard_rock'])
            ->willReturn($restaurant);

        $this->restaurantFacade
            ->expects($this->once())
            ->method('getEstimationForCurrentTime')
            ->with($restaurant)
            ->willReturn($estimation);

        $this->notificationFacade
            ->expects($this->once())
            ->method('envelopeAndSend')
            ->with(
                'Hard Rock Cafe',
                ['+40722222222', '+40733333333'],
                "Hard Rock Cafe will deliver your meal in 1 hours 30 minutes",
                123
            );

        $this->facade->sendNotification($notification, function (\DateInterval $interval) {
            return 123;
        });
    }

    public function tearDown(): void
    {
        unset($this->facade, $this->restaurantFacade, $this->notificationFacade);
        parent::tearDown();
    }
}
