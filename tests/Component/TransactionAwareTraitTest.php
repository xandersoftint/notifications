<?php
declare(strict_types=1);

namespace App\Tests\Component;

use App\Component\AbstractCRUDRepository;
use App\Component\TransactionAwareTrait;
use PHPUnit\Framework\TestCase;

class TransactionAwareTraitTest extends TestCase
{
    use TransactionAwareTrait;

    protected function setUp(): void
    {
        $this->setRepository($this
            ->getMockBuilder(AbstractCRUDRepository::class)
            ->disableOriginalConstructor()
            ->setMethods([])
            ->getMock());

        parent::setUp();
    }

    public function testEncapsulateTransactionSuccessfulExecution(): void
    {
        $this->repository->expects($this->once())->method('getTransactionLock');
        $this->repository->expects($this->once())->method('releaseTransactionLock');
        $this->repository->expects($this->never())->method('revertTransaction');

        $this->assertTrue(
            $this->encapsulateTransaction(function () {
                return true;
            })
        );
    }

    public function testEncapsulateTransactionUnSuccessfulExecution(): void
    {
        $this->expectException(\Exception::class);
        $this->expectExceptionMessage('Intended execution exception, for testing purposes');

        $this->repository->expects($this->once())->method('getTransactionLock');
        $this->repository->expects($this->never())->method('releaseTransactionLock');
        $this->repository->expects($this->once())->method('revertTransaction');

        $this->encapsulateTransaction(function () {
            throw new \Exception('Intended execution exception, for testing purposes');
        });
    }

    protected function tearDown(): void
    {
        unset ($this->repository);
        parent::tearDown();
    }
}
