<?php
declare(strict_types=1);

namespace App\Tests\Component;

use App\Component\ApiAwareRequestTrait;
use PHPUnit\Framework\TestCase;
use Symfony\Component\HttpFoundation\Request;

class ApiAwareRequestTraitTest extends TestCase
{
    use ApiAwareRequestTrait;

    public function testIsRequestApiCompatibleWithContentTypeJson(): void
    {
        $request = $this->getMockBuilder(Request::class)->setMethods(['getContentType'])->getMock();
        $request->expects($this->once())->method('getContentType')->willReturn('json');
        $this->assertTrue($this->isRequestApiCompatible($request));
    }

    public function testIsRequestApiCompatibleWithContentTypeText(): void
    {
        $request = $this->getMockBuilder(Request::class)->setMethods(['getContentType'])->getMock();
        $request->expects($this->once())->method('getContentType')->willReturn('text');
        $this->assertFalse($this->isRequestApiCompatible($request));
    }
}