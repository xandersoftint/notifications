<?php
declare(strict_types=1);

namespace App\Tests\Component;

use App\Component\AbstractCRUDRepository;
use App\Entity\EntityInterface;
use Doctrine\DBAL\Connection;
use Doctrine\ORM\EntityManagerInterface;
use Doctrine\ORM\Mapping\ClassMetadata;
use Doctrine\ORM\Persisters\Entity\EntityPersister;
use Doctrine\ORM\UnitOfWork;
use Doctrine\Persistence\ManagerRegistry;
use PHPUnit\Framework\TestCase;

class AbstractCRUDRepositoryTest extends TestCase
{
    private AbstractCRUDRepository $repository;
    private EntityPersister $persister;
    private EntityManagerInterface $entityManager;

    protected function setUp(): void
    {
        $this->persister = $this->getMockBuilder(EntityPersister::class)
            ->disableOriginalConstructor()
            ->getMock();

        $unitOfWorkMock = $this->getMockBuilder(UnitOfWork::class)
            ->disableOriginalConstructor()
            ->setMethods(['getEntityPersister'])
            ->getMock();
        $unitOfWorkMock->method('getEntityPersister')->willReturn($this->persister);

        $classMetadataStub = $this->getMockBuilder(ClassMetadata::class)
            ->disableOriginalConstructor()
            ->setMethods([])
            ->getMock();

        $this->entityManager = $this->getMockBuilder(EntityManagerInterface::class)->getMock();
        $this->entityManager->method('getClassMetadata')->willReturn($classMetadataStub);
        $this->entityManager->method('getUnitOfWork')->willReturn($unitOfWorkMock);

        $registryMock = $this->getMockBuilder(ManagerRegistry::class)->getMock();
        $registryMock->method('getManagerForClass')->willReturn($this->entityManager);

        $this->repository = new class ($registryMock, \get_class(new class {
        })) extends AbstractCRUDRepository {
        };

        parent::setUp();
    }

    public function testGetTransactionLockAndSaveEntityHavingNoActiveTransactionInitially(): AbstractCRUDRepository
    {
        $connectionMock = $this->getMockBuilder(Connection::class)
            ->disableOriginalConstructor()
            ->setMethods(['isTransactionActive'])
            ->getMock();

        $connectionMock->method('isTransactionActive')->willReturn(false);
        $this->entityManager->method('getConnection')->willReturn($connectionMock);

        $this->entityManager->expects($this->once())->method('beginTransaction');
        $this->entityManager->expects($this->once())->method('persist');
        $this->entityManager->expects($this->never())->method('flush');

        $this->repository->getTransactionLock();
        $this->repository->save(new class implements EntityInterface {
            public function getId(): int
            {
                return 1;
            }
        });

        return $this->repository;
    }

    /** @depends testGetTransactionLockAndSaveEntityHavingNoActiveTransactionInitially */
    public function testUpdateWithActiveTransaction(AbstractCRUDRepository $repository): void
    {
        /**
         * Should detect the inLock variable set to true and do nothing
         */
        $this->assertInstanceOf(AbstractCRUDRepository::class, $repository->update());
    }

    public function testReleaseTransactionLockAndUpdateEntityHavingActiveTransactionInitially(): void
    {
        $connectionMock = $this->getMockBuilder(Connection::class)
            ->disableOriginalConstructor()
            ->setMethods(['isTransactionActive'])
            ->getMock();

        $connectionMock->method('isTransactionActive')->willReturnOnConsecutiveCalls(true);
        $this->entityManager->method('getConnection')->willReturn($connectionMock);

        $this->entityManager->expects($this->never())->method('beginTransaction');
        $this->entityManager->expects($this->once())->method('commit');
        $this->entityManager->expects($this->once())->method('flush');

        $this->repository->update();
        $this->repository->releaseTransactionLock();
    }

    public function testRevertTransactionLockHavingActiveTransactionInitially(): void
    {
        $connectionMock = $this->getMockBuilder(Connection::class)
            ->disableOriginalConstructor()
            ->setMethods(['isTransactionActive'])
            ->getMock();

        $connectionMock->method('isTransactionActive')->willReturn(true);
        $this->entityManager->method('getConnection')->willReturn($connectionMock);

        $this->entityManager->expects($this->never())->method('beginTransaction');
        $this->entityManager->expects($this->never())->method('flush');

        $this->entityManager->expects($this->once())->method('rollback');

        $this->repository->revertTransaction();
    }

    public function testReleaseTransactionLockHavingActiveTransactionInitially(): void
    {
        $connectionMock = $this->getMockBuilder(Connection::class)
            ->disableOriginalConstructor()
            ->setMethods(['isTransactionActive'])
            ->getMock();

        $connectionMock->method('isTransactionActive')->willReturn(true);
        $this->entityManager->method('getConnection')->willReturn($connectionMock);

        $this->entityManager->expects($this->never())->method('beginTransaction');
        $this->entityManager->expects($this->once())->method('flush');
        $this->entityManager->expects($this->once())->method('commit');

        $this->repository->releaseTransactionLock();
    }

    protected function tearDown(): void
    {
        unset($this->repository, $this->entityManager, $this->entityManager);
        parent::tearDown();
    }
}
