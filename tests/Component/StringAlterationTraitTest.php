<?php
declare(strict_types=1);

namespace App\Tests\Component;

use App\Component\StringAlterationTrait;
use PHPUnit\Framework\TestCase;

class StringAlterationTraitTest extends TestCase
{
    use StringAlterationTrait;

    public function testHydrateUri(): void
    {
        $result = $this->hydrateUri('aaaa{test}bbbb{test2}', [
            'test' => 'ccc',
            'test2' => 'ddd',
        ]);

        $this->assertEquals('aaaacccbbbbddd', $result);
    }
}
