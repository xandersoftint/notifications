<?php
declare(strict_types=1);

namespace App\Tests\Entity;

use App\Entity\DeliveryInterval;
use App\Exception\PropertyUndefinedException;
use PHPUnit\Framework\TestCase;

class DeliveryIntervalTest extends TestCase
{
    public function testGetBeginAtHasDefaultSet(): void
    {
        $entity = new DeliveryInterval();
        $this->assertEquals(\DateTime::createFromFormat('!H:i', '00:00'), $entity->getBeginAt());
    }

    public function testGetEndAtHasDefaultSet(): void
    {
        $entity = new DeliveryInterval();
        $this->assertEquals(\DateTime::createFromFormat('!H:i', '00:00'), $entity->getEndAt());
    }

    public function testGetBeginAtReturnsValueIfSet(): void
    {
        $entity = new DeliveryInterval();
        $entity->setBeginAt(new \DateTime('2020-01-01'));
        $this->assertEquals('2020-01-01', $entity->getBeginAt()->format('Y-m-d'));
    }

    public function testGetEndAtReturnsValueIfSet(): void
    {
        $entity = new DeliveryInterval();
        $entity->setEndAt(new \DateTime('2020-01-01'));
        $this->assertEquals('2020-01-01', $entity->getEndAt()->format('Y-m-d'));
    }
}
