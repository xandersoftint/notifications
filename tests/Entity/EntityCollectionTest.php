<?php
declare(strict_types=1);

namespace App\Tests\Entity;

use App\Entity\EntityCollection;
use App\Entity\Sms;
use App\Entity\SmsTransaction;
use App\Exception\TypeUnsupportedException;
use PHPUnit\Framework\TestCase;

class EntityCollectionTest extends TestCase
{
    public function testConstructorWithValidObjects(): void
    {
        $collection = new EntityCollection([
            new SmsTransaction(),
            new SmsTransaction(),
            new Sms(),
        ]);

        $this->assertCount(3, $collection);
    }

    public function testConstructorWithInvalidObjects(): void
    {
        $this->expectException(TypeUnsupportedException::class);

        new EntityCollection([
            new SmsTransaction(),
            new \stdClass(),
            new Sms(),
        ]);
    }

    public function testOffsetSetWithValidObject(): void
    {
        $input = new SmsTransaction();
        $collection = new EntityCollection();
        $collection->offsetSet('key', $input);

        $this->assertCount(1, $collection);
        $this->assertEquals($input, $collection->offsetGet('key'));
    }

    public function testOffsetSetWithInValidObject(): void
    {
        $this->expectException(TypeUnsupportedException::class);

        $collection = new EntityCollection();
        $collection->offsetSet('key', new \stdClass());
    }

    public function testExchangeWithValidObjects(): void
    {
        $collection = new EntityCollection();
        $collection->exchangeArray([
            new SmsTransaction(),
            new Sms(),
            new SmsTransaction(),
        ]);

        $this->assertCount(3, $collection);
    }

    public function testExchangeWithInvalidObjects(): void
    {
        $this->expectException(TypeUnsupportedException::class);

        $collection = new EntityCollection();
        $collection->exchangeArray([
            new SmsTransaction(),
            new \stdClass(),
            new SmsTransaction(),
        ]);
    }
}
