<?php
declare(strict_types=1);

namespace App\Tests\Entity;

use App\Entity\DeliveryInterval;
use App\Entity\DeliveryIntervalCollection;
use App\Exception\TypeUnsupportedException;
use PHPUnit\Framework\TestCase;

class DeliveryIntervalCollectionTest extends TestCase
{
    public function testConstructorWithValidObjects(): void
    {
        $collection = new DeliveryIntervalCollection([
            new DeliveryInterval(),
            new DeliveryInterval(),
            new DeliveryInterval(),
        ]);

        $this->assertCount(3, $collection);
    }

    public function testConstructorWithInvalidObjects(): void
    {
        $this->expectException(TypeUnsupportedException::class);

        new DeliveryIntervalCollection([
            new DeliveryInterval(),
            new \stdClass(),
            new DeliveryInterval(),
        ]);
    }

    public function testOffsetSetWithValidObject(): void
    {
        $input = new DeliveryInterval();
        $collection = new DeliveryIntervalCollection();
        $collection->offsetSet('key', $input);

        $this->assertCount(1, $collection);
        $this->assertEquals($input, $collection->offsetGet('key'));
    }

    public function testOffsetSetWithInValidObject(): void
    {
        $this->expectException(TypeUnsupportedException::class);

        $collection = new DeliveryIntervalCollection();
        $collection->offsetSet('key', new \stdClass());
    }

    public function testExchangeWithValidObjects(): void
    {
        $collection = new DeliveryIntervalCollection();
        $collection->exchangeArray([
            new DeliveryInterval(),
            new DeliveryInterval(),
            new DeliveryInterval(),
        ]);

        $this->assertCount(3, $collection);
    }

    public function testExchangeWithInvalidObjects(): void
    {
        $this->expectException(TypeUnsupportedException::class);

        $collection = new DeliveryIntervalCollection();
        $collection->exchangeArray([
            new DeliveryInterval(),
            new \stdClass(),
            new DeliveryInterval(),
        ]);
    }
}
