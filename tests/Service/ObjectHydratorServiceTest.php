<?php
declare(strict_types=1);

namespace App\Tests\Service;

use App\Dto\DtoInterface;
use App\Dto\Restaurant;
use App\Entity\DeliveryInterval;
use App\Entity\EntityInterface;
use App\Exception\CoreException;
use App\Hydrator\DtoHydrator;
use App\Hydrator\EntityHydrator;
use App\Service\ObjectHydratorService;
use PHPUnit\Framework\TestCase;

class ObjectHydratorServiceTest extends TestCase
{
    public function testHydrateEntity(): void
    {
        $hydrator = new ObjectHydratorService();
        $hydrator->addHydrator(EntityInterface::class, new EntityHydrator());
        $hydrator->addHydrator(DtoInterface::class, new DtoHydrator());

        $object = $hydrator->hydrate(new DeliveryInterval(), ['estimation' => new \DateInterval('PT30M')]);
        $this->assertEquals(30, $object->getEstimation()->i);
    }

    public function testHydrateDto(): void
    {
        $hydrator = new ObjectHydratorService();
        $hydrator->addHydrator(EntityInterface::class, new EntityHydrator());
        $hydrator->addHydrator(DtoInterface::class, new DtoHydrator());

        $object = $hydrator->hydrate(new Restaurant(), ['code' => 'restaurant_code']);
        $this->assertEquals('restaurant_code', $object->code);
    }

    public function testHydrateUnknown(): void
    {
        $this->expectException(CoreException::class);

        $hydrator = new ObjectHydratorService();
        $hydrator->addHydrator(EntityInterface::class, new EntityHydrator());
        $hydrator->addHydrator(DtoAbstract::class, new DtoHydrator());

        $hydrator->hydrate(new \stdClass(), ['id' => 10]);
    }
}
