<?php
declare(strict_types=1);

namespace App\Tests\Service;

use App\Exception\IntegrationException;
use App\Exception\IntegrationRespondedException;
use App\Service\EnvironmentService;
use App\Service\IntegrationService;
use App\Service\ObjectHydratorService;
use App\Service\TypeConverterService;
use PHPUnit\Framework\TestCase;
use Psr\Log\LoggerInterface;
use Symfony\Component\HttpClient\Exception\ClientException;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Serializer\NameConverter\CamelCaseToSnakeCaseNameConverter;
use Symfony\Component\Serializer\SerializerInterface;
use Symfony\Contracts\HttpClient\HttpClientInterface;
use App\Dto\Integration\Request as IntegrationRequest;
use Symfony\Contracts\HttpClient\ResponseInterface;

class IntegrationServiceTest extends TestCase
{
    protected function setUp(): void
    {
        new EnvironmentService('/', 'en');
        parent::setUp();
    }

    public function testExecuteWithIntegrationSuccess(): void
    {
        $serializerStub = $this->getMockBuilder(SerializerInterface::class)->setMethods([])->getMock();
        $hydratorStub = $this->getMockBuilder(ObjectHydratorService::class)->setMethods([])->getMock();

        $logger = $this->getMockBuilder(LoggerInterface::class)->setMethods([])->getMock();
        $logger->expects($this->exactly(2))->method('info');

        $response = $this->getMockBuilder(ResponseInterface::class)->setMethods([])->getMock();
        $response->expects($this->once())->method('getContent')->willReturn('{"status": "success"}');
        $response->expects($this->exactly(2))->method('getHeaders')->with(false)->willReturn(['Content-Type' => 'json']);

        $client = $this->getMockBuilder(HttpClientInterface::class)->setMethods([])->getMock();
        $client->expects($this->once())->method('request')->with(
            Request::METHOD_POST,
            'http://sms-notification-system/send',
            [
                'headers' => [
                    'Accept-Language'=> 'en',
                 ],
                'body' => ['message' => 'Test message']
            ]
        )->willReturn($response);

        $converter = new TypeConverterService(
            $serializerStub,
            $hydratorStub,
            new CamelCaseToSnakeCaseNameConverter()
        );

        $request = new IntegrationRequest('http://sms-notification-system');
        $request->init(Request::METHOD_POST, 'send');
        $request->setContent([
            'message' => 'Test message',
        ]);

        $integrationService = new IntegrationService($client, $logger, $converter);
        $response = $integrationService->execute($request);
        $this->assertEquals(["status" => 'success'], $response->contents);
        $this->assertEquals(['Content-Type' => 'json'], $response->headers);
    }

    public function testExecuteWithIntegrationSuccessWithRequestContentOfTypeJson(): void
    {
        $serializerStub = $this->getMockBuilder(SerializerInterface::class)->setMethods([])->getMock();
        $hydratorStub = $this->getMockBuilder(ObjectHydratorService::class)->setMethods([])->getMock();

        $logger = $this->getMockBuilder(LoggerInterface::class)->setMethods([])->getMock();
        $logger->expects($this->exactly(2))->method('info');

        $response = $this->getMockBuilder(ResponseInterface::class)->setMethods([])->getMock();
        $response->expects($this->once())->method('getContent')->willReturn('{"status": "success"}');
        $response->expects($this->exactly(2))->method('getHeaders')->with(false)->willReturn(['Content-Type' => 'json']);

        $client = $this->getMockBuilder(HttpClientInterface::class)->setMethods([])->getMock();
        $client->expects($this->once())->method('request')->with(
            Request::METHOD_POST,
            'http://sms-notification-system/send',
            [
                'headers' => [
                    'Accept-Language'=> 'en',
                ],
                'json' => ['message' => 'Test message']
            ]
        )->willReturn($response);

        $converter = new TypeConverterService(
            $serializerStub,
            $hydratorStub,
            new CamelCaseToSnakeCaseNameConverter()
        );

        $request = new IntegrationRequest('http://sms-notification-system');
        $request->init(Request::METHOD_POST, 'send');
        $request->setContent([
            'message' => 'Test message',
        ]);

        $integrationService = new IntegrationService($client, $logger, $converter);
        $response = $integrationService->execute($request->toJson());
        $this->assertEquals(["status" => 'success'], $response->contents);
        $this->assertEquals(['Content-Type' => 'json'], $response->headers);
    }

    public function testExecuteWithIntegrationButClientOfferedResponse(): void
    {
        $this->expectException(IntegrationRespondedException::class);
        $this->expectExceptionCode(400);

        $serializerStub = $this->getMockBuilder(SerializerInterface::class)->setMethods([])->getMock();
        $hydratorStub = $this->getMockBuilder(ObjectHydratorService::class)->setMethods([])->getMock();

        $logger = $this->getMockBuilder(LoggerInterface::class)->setMethods([])->getMock();
        $logger->expects($this->once())->method('info');
        $logger->expects($this->once())->method('warning');

        $clientReponse = $this->getMockBuilder(ResponseInterface::class)->setMethods([])->getMock();
        $clientReponse->expects($this->once())->method('getContent')->with(false)->willReturn('{"status": "failed"}');
        $clientReponse->expects($this->once())->method('getHeaders')->with(false)->willReturn(['Content-Type' => 'json']);
        $clientReponse->method('getInfo')->willReturnOnConsecutiveCalls(400, 'http://sms-notification-system/send', []);

        $response = $this->getMockBuilder(ResponseInterface::class)->setMethods([])->getMock();
        $response->expects($this->once())->method('getContent')->willThrowException(new ClientException($clientReponse));

        $client = $this->getMockBuilder(HttpClientInterface::class)->setMethods([])->getMock();
        $client->expects($this->once())->method('request')->with(
            Request::METHOD_POST,
            'http://sms-notification-system/send',
            [
                'headers' => [
                    'Accept-Language'=> 'en',
                ],
                'body' => ['message' => 'Test message']
            ]
        )->willReturn($response);

        $converter = new TypeConverterService(
            $serializerStub,
            $hydratorStub,
            new CamelCaseToSnakeCaseNameConverter()
        );

        $request = new IntegrationRequest('http://sms-notification-system');
        $request->init(Request::METHOD_POST, 'send');
        $request->setContent([
            'message' => 'Test message',
        ]);

        $integrationService = new IntegrationService($client, $logger, $converter);
        $integrationService->execute($request);
    }

    public function testExecuteWithIntegrationButClientDidNotRespond(): void
    {
        $this->expectException(IntegrationException::class);

        $serializerStub = $this->getMockBuilder(SerializerInterface::class)->setMethods([])->getMock();
        $hydratorStub = $this->getMockBuilder(ObjectHydratorService::class)->setMethods([])->getMock();

        $logger = $this->getMockBuilder(LoggerInterface::class)->setMethods([])->getMock();
        $logger->expects($this->once())->method('info');

        $response = $this->getMockBuilder(ResponseInterface::class)->setMethods([])->getMock();
        $response->expects($this->once())->method('getContent')->willThrowException(new \Exception());

        $client = $this->getMockBuilder(HttpClientInterface::class)->setMethods([])->getMock();
        $client->expects($this->once())->method('request')->with(
            Request::METHOD_POST,
            'http://sms-notification-system/send',
            [
                'headers' => [
                    'Accept-Language'=> 'en',
                ],
                'body' => ['message' => 'Test message']
            ]
        )->willReturn($response);

        $converter = new TypeConverterService(
            $serializerStub,
            $hydratorStub,
            new CamelCaseToSnakeCaseNameConverter()
        );

        $request = new IntegrationRequest('http://sms-notification-system');
        $request->init(Request::METHOD_POST, 'send');
        $request->setContent([
            'message' => 'Test message',
        ]);

        $integrationService = new IntegrationService($client, $logger, $converter);
        $integrationService->execute($request);
    }
}
