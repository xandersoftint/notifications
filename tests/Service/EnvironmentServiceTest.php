<?php
declare(strict_types=1);

namespace App\Tests\Service;

use App\Service\EnvironmentService;
use PHPUnit\Framework\TestCase;

class EnvironmentServiceTest extends TestCase
{
    public function testGetLocaleWhenNotSet()
    {
        $environmentService = new EnvironmentService('/', 'en');
        $this->assertEquals('en', $environmentService->getLocale());
    }

    public function testGetLocaleWhenSet()
    {
        $environmentService = new EnvironmentService('/', 'en');
        $environmentService->setLocale('ro');
        $this->assertEquals('ro', $environmentService->getLocale());
    }
}
