<?php
declare(strict_types=1);

namespace App\Tests\Service;

use App\Entity\EntityInterface;
use App\Entity\NotificationInterface;
use App\Entity\Sms;
use App\Exception\ObjectValidationException;
use App\Hydrator\EntityHydrator;
use App\Repository\SmsRepository;
use App\Service\ObjectHydratorService;
use App\Service\SmsService;
use App\Service\TypeConverterService;
use PHPUnit\Framework\TestCase;
use Symfony\Component\Validator\ConstraintViolationListInterface;
use Symfony\Component\Validator\Validator\ValidatorInterface;

class SmsServiceTest extends TestCase
{
    private SmsService $service;
    private SmsRepository $repository;
    private ValidatorInterface $validator;
    private TypeConverterService $converter;

    protected function setUp(): void
    {
        $this->repository = $this->getMockBuilder(SmsRepository::class)
            ->disableOriginalConstructor()
            ->getMock();
        $this->validator = $this->getMockBuilder(ValidatorInterface::class)->setMethods([])->getMock();
        $this->converter = $this->getMockBuilder(TypeConverterService::class)->disableOriginalConstructor()->getMock();

        $this->service = new SmsService($this->repository);
        $this->service->setValidator($this->validator);
        $this->service->setConverter($this->converter);
        parent::setUp();
    }

    public function testCreate(): void
    {
        $input = [
            'payload' => 'Hard Rock Cafe will deliver your meal in 30 minutes',
            'destination' => '+40722222222',
        ];

        $inputGeneratedEntity = (new Sms())
            ->setMessage('Hard Rock Cafe will deliver your meal in 30 minutes')
            ->setDestination('+40722222222');

        $this->converter->expects($this->once())
            ->method('arrayToEntity')
            ->with($input)
            ->willReturn($inputGeneratedEntity);

        $this->repository->expects($this->once())->method('save')->with($inputGeneratedEntity)->willReturnArgument(0);

        $constraintViolationListMock = $this
            ->getMockBuilder(ConstraintViolationListInterface::class)
            ->setMethods([])
            ->getMock();
        $constraintViolationListMock->expects($this->once())->method('count')->willReturn(0);

        $this->validator
            ->expects($this->once())
            ->method('validate')
            ->with($inputGeneratedEntity)
            ->willReturn($constraintViolationListMock);


        /** @var Sms $entity */
        $entity = $this->service->create($input);
        $this->assertEquals('Hard Rock Cafe will deliver your meal in 30 minutes', $entity->getMessage());
        $this->assertEquals('+40722222222', $entity->getDestination());
        $this->assertEquals(NotificationInterface::STATUS_NEW, $entity->getStatus());
    }

    public function testCreateWithValidationException(): void
    {
        $this->expectException(ObjectValidationException::class);

        $input = [
            'payload' => 'Hard Rock Cafe will deliver your meal in 30 minutes',
            'destination' => '+40722222222',
        ];

        $inputGeneratedEntity = (new Sms())
            ->setMessage('Hard Rock Cafe will deliver your meal in 30 minutes')
            ->setDestination('+40722222222');

        $this->converter->expects($this->once())
            ->method('arrayToEntity')
            ->with($input)
            ->willReturn($inputGeneratedEntity);

        $constraintViolationListMock = $this
            ->getMockBuilder(ConstraintViolationListInterface::class)
            ->setMethods([])
            ->getMock();
        $constraintViolationListMock->expects($this->once())->method('count')->willReturn(1);

        $this->validator
            ->expects($this->once())
            ->method('validate')
            ->with($inputGeneratedEntity)
            ->willReturn($constraintViolationListMock);

        $this->repository->expects($this->never())->method('save');
        $this->service->create($input);
    }

    public function testGetById(): void
    {
        $this->repository->expects($this->once())->method('find')->with(1)->willReturn(new Sms());
        $entity = $this->service->get(1);
        $this->assertInstanceOf(Sms::class, $entity);
    }

    public function testDelete(): void
    {
        $input = new Sms();
        $this->repository->expects($this->once())->method('remove')->with($input);
        $this->service->delete($input);
    }

    protected function tearDown(): void
    {
        unset($this->repository, $this->service, $this->validator, $this->converter);
        parent::tearDown();
    }

    public function testUpdateWithStatusChange(): void
    {
        $this->repository->expects($this->once())->method('save')->willReturnArgument(0);

        $constraintViolationListMock = $this
            ->getMockBuilder(ConstraintViolationListInterface::class)
            ->setMethods([])
            ->getMock();
        $constraintViolationListMock->expects($this->once())->method('count')->willReturn(0);

        $this->validator
            ->expects($this->once())
            ->method('validate')
            ->willReturn($constraintViolationListMock);

        $hydrator = new ObjectHydratorService();
        $hydrator->addHydrator(EntityInterface::class, new EntityHydrator());
        $this->service->setHydrator($hydrator);

        $entity = (new Sms())
            ->setMessage('Hard Rock Cafe will deliver your meal in 30 minutes')
            ->setDestination('+40722222222');

        /** @var Sms $result */
        $result = $this->service->update(
            $entity,
            [
                'status' => NotificationInterface::STATUS_SENT
            ]
        );

        $this->assertEquals('Hard Rock Cafe will deliver your meal in 30 minutes', $result->getMessage());
        $this->assertEquals('+40722222222', $result->getDestination());
        $this->assertEquals(NotificationInterface::STATUS_SENT, $result->getStatus());
    }
}
