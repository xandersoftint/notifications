<?php
declare(strict_types=1);

namespace App\Tests\Service;

use App\Entity\DeliveryInterval;
use App\Entity\Restaurant;
use App\Exception\DeliveryIntervalNotFoundException;
use App\Exception\ObjectValidationException;
use App\Repository\DeliveryIntervalRepository;
use App\Service\DeliveryIntervalService;
use App\Service\TypeConverterService;
use PHPUnit\Framework\TestCase;
use Symfony\Component\Validator\ConstraintViolationListInterface;
use Symfony\Component\Validator\Validator\ValidatorInterface;

class DeliveryIntervalServiceTest extends TestCase
{
    private DeliveryIntervalService $service;
    private DeliveryIntervalRepository $repository;
    private ValidatorInterface $validator;
    private TypeConverterService $converter;

    protected function setUp(): void
    {
        $this->repository = $this->getMockBuilder(DeliveryIntervalRepository::class)
            ->disableOriginalConstructor()
            ->getMock();
        $this->validator = $this->getMockBuilder(ValidatorInterface::class)->setMethods([])->getMock();
        $this->converter = $this->getMockBuilder(TypeConverterService::class)->disableOriginalConstructor()->getMock();

        $this->service = new DeliveryIntervalService($this->repository);
        $this->service->setValidator($this->validator);
        $this->service->setConverter($this->converter);
        parent::setUp();
    }

    public function testCreate(): void
    {
        $input = [
            'restaurant' => new Restaurant(),
            'deliveryEstimation' => new \DateInterval('PT30M'),
        ];

        $inputGeneratedEntity = (new DeliveryInterval())
            ->setRestaurant($input['restaurant'])
            ->setEstimation($input['deliveryEstimation']);

        $this->converter->expects($this->once())
            ->method('arrayToEntity')
            ->with($input)
            ->willReturn($inputGeneratedEntity);

        $this->repository->expects($this->once())->method('save')->with($inputGeneratedEntity)->willReturnArgument(0);

        $constraintViolationListMock = $this
            ->getMockBuilder(ConstraintViolationListInterface::class)
            ->setMethods([])
            ->getMock();
        $constraintViolationListMock->expects($this->once())->method('count')->willReturn(0);

        $this->validator
            ->expects($this->once())
            ->method('validate')
            ->with($inputGeneratedEntity)
            ->willReturn($constraintViolationListMock);


        /** @var DeliveryInterval $entity */
        $entity = $this->service->create($input);
        $this->assertInstanceOf(Restaurant::class, $entity->getRestaurant());
        $this->assertEquals(30, $entity->getEstimation()->i);
    }

    public function testCreateWithValidationException(): void
    {
        $this->expectException(ObjectValidationException::class);

        $input = [
            'estimation' => new \DateInterval('PT30M'),
        ];
        $inputGeneratedEntity = (new DeliveryInterval())
            ->setEstimation($input['estimation']);

        $this->converter->expects($this->once())
            ->method('arrayToEntity')
            ->with($input)
            ->willReturn($inputGeneratedEntity);

        $constraintViolationListMock = $this
            ->getMockBuilder(ConstraintViolationListInterface::class)
            ->setMethods([])
            ->getMock();
        $constraintViolationListMock->expects($this->once())->method('count')->willReturn(1);

        $this->validator
            ->expects($this->once())
            ->method('validate')
            ->with($inputGeneratedEntity)
            ->willReturn($constraintViolationListMock);

        $this->repository->expects($this->never())->method('save');

        $this->service->create($input);
    }

    public function testGetById(): void
    {
        $this->repository->expects($this->once())->method('find')->with(1)->willReturn(new DeliveryInterval());
        $entity = $this->service->get(1);
        $this->assertInstanceOf(DeliveryInterval::class, $entity);
    }

    public function testGetByRestaurantAndTimeFindsInterval(): void
    {
        $currentTime = new \DateTime();

        $this->repository
            ->expects($this->once())
            ->method('findByRestaurantAndTime')
            ->with(new Restaurant(), $currentTime)
            ->willReturn(new DeliveryInterval());

        $this->repository
            ->expects($this->never())
            ->method('findDefaultByRestaurant');

        $entity = $this->service->getByRestaurantAndTime(new Restaurant(), $currentTime);
        $this->assertInstanceOf(DeliveryInterval::class, $entity);
    }

    public function testGetByRestaurantAndTimeFindsDefaultInterval(): void
    {
        $currentTime = new \DateTime();

        $this->repository
            ->expects($this->once())
            ->method('findByRestaurantAndTime')
            ->with(new Restaurant(), $currentTime)
            ->willThrowException(new DeliveryIntervalNotFoundException());

        $this->repository
            ->expects($this->once())
            ->method('findDefaultByRestaurant')
            ->with(new Restaurant())
            ->willReturn(new DeliveryInterval());

        $entity = $this->service->getByRestaurantAndTime(new Restaurant(), $currentTime);
        $this->assertInstanceOf(DeliveryInterval::class, $entity);
    }

    public function testDelete(): void
    {
        $input = new DeliveryInterval();
        $this->repository->expects($this->once())->method('remove')->with($input);
        $this->service->delete($input);
    }

    protected function tearDown(): void
    {
        unset($this->repository, $this->service, $this->validator, $this->converter);
        parent::tearDown();
    }
}
