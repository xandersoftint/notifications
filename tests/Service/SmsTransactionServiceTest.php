<?php
declare(strict_types=1);

namespace App\Tests\Service;

use App\Entity\EntityCollection;
use App\Entity\PaginatedCollection;
use App\Entity\SmsTransaction;
use App\Exception\ObjectValidationException;
use App\Repository\SmsTransactionRepository;
use App\Service\SmsTransactionService;
use App\Service\TypeConverterService;
use PHPUnit\Framework\TestCase;
use Symfony\Component\Validator\ConstraintViolationListInterface;
use Symfony\Component\Validator\Validator\ValidatorInterface;

class SmsTransactionServiceTest extends TestCase
{
    private SmsTransactionService $service;
    private SmsTransactionRepository $repository;
    private ValidatorInterface $validator;
    private TypeConverterService $converter;

    protected function setUp(): void
    {
        $this->repository = $this->getMockBuilder(SmsTransactionRepository::class)
            ->disableOriginalConstructor()
            ->setMethods(['listErrorsBy', 'listBy', 'save', 'find', 'remove'])
            ->getMock();

        $this->validator = $this->getMockBuilder(ValidatorInterface::class)->setMethods([])->getMock();
        $this->converter = $this->getMockBuilder(TypeConverterService::class)->disableOriginalConstructor()->getMock();

        $this->service = new SmsTransactionService($this->repository);
        $this->service->setValidator($this->validator);
        $this->service->setConverter($this->converter);
        parent::setUp();
    }

    public function testCreate(): void
    {
        $input = [
            'status' => 200,
            'response' => 'success',
        ];

        $inputGeneratedEntity = (new SmsTransaction())
            ->setStatus(200)
            ->setResponse(['message' => 'success']);

        $this->converter->expects($this->once())
            ->method('arrayToEntity')
            ->with($input)
            ->willReturn($inputGeneratedEntity);

        $this->repository->expects($this->once())->method('save')->with($inputGeneratedEntity)->willReturnArgument(0);

        $constraintViolationListMock = $this
            ->getMockBuilder(ConstraintViolationListInterface::class)
            ->setMethods([])
            ->getMock();
        $constraintViolationListMock->expects($this->once())->method('count')->willReturn(0);

        $this->validator
            ->expects($this->once())
            ->method('validate')
            ->with($inputGeneratedEntity)
            ->willReturn($constraintViolationListMock);


        /** @var SmsTransaction $entity */
        $entity = $this->service->create($input);
        $this->assertEquals(200, $entity->getStatus());
        $this->assertEquals(['message' => 'success'], $entity->getResponse());
    }

    public function testCreateWithValidationException(): void
    {
        $this->expectException(ObjectValidationException::class);

        $input = [
            'status' => 200,
            'response' => 'success',
        ];

        $inputGeneratedEntity = (new SmsTransaction())
            ->setStatus(200)
            ->setResponse(['message' => 'success']);

        $this->converter->expects($this->once())
            ->method('arrayToEntity')
            ->with($input)
            ->willReturn($inputGeneratedEntity);

        $constraintViolationListMock = $this
            ->getMockBuilder(ConstraintViolationListInterface::class)
            ->setMethods([])
            ->getMock();
        $constraintViolationListMock->expects($this->once())->method('count')->willReturn(1);

        $this->validator
            ->expects($this->once())
            ->method('validate')
            ->with($inputGeneratedEntity)
            ->willReturn($constraintViolationListMock);

        $this->repository->expects($this->never())->method('save');
        $this->service->create($input);
    }

    public function testGetById(): void
    {
        $this->repository->expects($this->once())->method('find')->with(1)->willReturn(new SmsTransaction());
        $entity = $this->service->get(1);
        $this->assertInstanceOf(SmsTransaction::class, $entity);
    }

    public function testDelete(): void
    {
        $input = new SmsTransaction();
        $this->repository->expects($this->once())->method('remove')->with($input);
        $this->service->delete($input);
    }

    public function testListErrors()
    {
        $now = new \DateTime();
        $expected = clone $now;
        $expected->sub(new \DateInterval('P2D'));

        $this->repository->expects($this->once())->method('listErrorsBy')
            ->with($expected)
            ->willReturn(
                new PaginatedCollection(
                    new EntityCollection([
                        new SmsTransaction(),
                        new SmsTransaction(),
                    ]),
                    4,
                    2
                )
            );

        $response = $this->service->listErrors(2, $now);
        $this->assertCount(2, $response->getCollection());
        $this->assertEquals(4, $response->getTotal());
        $this->assertEquals(2, $response->getFiltered());
    }

    public function testList()
    {
        $now = new \DateTime();
        $expected = clone $now;
        $expected->sub(new \DateInterval('P2D'));

        $this->repository->expects($this->once())->method('listBy')
            ->with($expected, 1, 20)
            ->willReturn(
                new PaginatedCollection(
                    new EntityCollection([
                        new SmsTransaction(),
                        new SmsTransaction(),
                    ]),
                    3,
                    2
                )
            );

        $response = $this->service->list(2, 1, 20, '', $now);
        $this->assertCount(2, $response->getCollection());
        $this->assertEquals(3, $response->getTotal());
        $this->assertEquals(2, $response->getFiltered());
    }

    protected function tearDown(): void
    {
        unset($this->repository, $this->service, $this->validator, $this->converter);
        parent::tearDown();
    }
}
