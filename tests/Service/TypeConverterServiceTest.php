<?php
declare(strict_types=1);

namespace App\Tests\Service;

use App\Dto\ApiResponse;
use App\Dto\DtoInterface;
use App\Dto\Restaurant;
use App\Entity\DeliveryInterval;
use App\Entity\EntityInterface;
use App\Hydrator\DtoHydrator;
use App\Hydrator\EntityHydrator;
use App\Service\ObjectHydratorService;
use App\Service\TypeConverterService;
use PHPUnit\Framework\TestCase;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\Serializer\NameConverter\CamelCaseToSnakeCaseNameConverter;
use Symfony\Component\Serializer\Normalizer\AbstractNormalizer;
use Symfony\Component\Serializer\SerializerInterface;

class TypeConverterServiceTest extends TestCase
{
    public function testArrayToJson(): void
    {
        $serializer = $this->getMockBuilder(SerializerInterface::class)->getMock();
        $serializer
            ->method('serialize')
            ->with([1 => 'first_element', 2 => 'second_element'], 'json', [])
            ->willReturn('{"1": "first_element", "2": "second_element"}');

        $hydratorStub = $this->getMockBuilder(ObjectHydratorService::class)->setMethods([])->getMock();
        $nameConverter = new CamelCaseToSnakeCaseNameConverter();
        $converter = new TypeConverterService($serializer, $hydratorStub, $nameConverter);

        $this->assertEquals(
            '{"1": "first_element", "2": "second_element"}',
            $converter->arrayToJson([1 => 'first_element', 2 => 'second_element']),
        );
    }

    public function testJsonToArray(): void
    {
        $serializer = $this->getMockBuilder(SerializerInterface::class)->getMock();
        $hydratorStub = $this->getMockBuilder(ObjectHydratorService::class)->setMethods([])->getMock();
        $nameConverter = new CamelCaseToSnakeCaseNameConverter();
        $converter = new TypeConverterService($serializer, $hydratorStub, $nameConverter);

        $this->assertEquals(
            [1 => 'first_element', 2 => 'second_element'],
            $converter->jsonToArray('{"1": "first_element", "2": "second_element"}')
        );
    }

    public function testObjectToArray(): void
    {
        $serializer = $this->getMockBuilder(SerializerInterface::class)->setMethods(['serialize', 'deserialize'])->getMock();
        $hydratorStub = $this->getMockBuilder(ObjectHydratorService::class)->setMethods([])->getMock();
        $nameConverter = new CamelCaseToSnakeCaseNameConverter();

        $serializer->expects($this->once())->method('serialize')->with(
            (object)[1 => 'first_element', 'testKey' => 'second_element'],
            'json'
        )->willReturn('{"1": "first_element", "test_key": "second_element"}');

        $converter = new TypeConverterService($serializer, $hydratorStub, $nameConverter);

        $this->assertEquals(
            [1 => 'first_element', 'testKey' => 'second_element'],
            $converter->objectToArray((object)[1 => 'first_element', 'testKey' => 'second_element'])
        );
    }

    public function testJsonToArrayWithStringKeyNames(): void
    {
        $serializer = $this->getMockBuilder(SerializerInterface::class)->getMock();
        $hydratorStub = $this->getMockBuilder(ObjectHydratorService::class)->setMethods([])->getMock();
        $nameConverter = new CamelCaseToSnakeCaseNameConverter();
        $converter = new TypeConverterService($serializer, $hydratorStub, $nameConverter);

        $this->assertEquals(
            ['firstKey' => 'first_element', 'secondKey' => 'second_element'],
            $converter->jsonToArray('{"first_key": "first_element", "second_key": "second_element"}')
        );
    }

    public function testJsonToArrayWithStringKeyNamesAndArrayValues(): void
    {
        $serializer = $this->getMockBuilder(SerializerInterface::class)->getMock();
        $hydratorStub = $this->getMockBuilder(ObjectHydratorService::class)->setMethods([])->getMock();
        $nameConverter = new CamelCaseToSnakeCaseNameConverter();
        $converter = new TypeConverterService($serializer, $hydratorStub, $nameConverter);


        $input = '{"first_key": "first_element", "second_key": "second_element", "third_key": {"second_child_key": "second_child_value"}}';
        $this->assertEquals(
            [
                'firstKey' => 'first_element',
                'secondKey' => 'second_element',
                'thirdKey' => ['secondChildKey' => 'second_child_value']
            ],
            $converter->jsonToArray($input)
        );
    }

    public function testArrayToEntity(): void
    {
        $input = ['id' => 1, 'estimation' => new \DateInterval('PT30M')];

        $serializer = $this->getMockBuilder(SerializerInterface::class)->getMock();
        $serializer
            ->method('serialize')
            ->with(['id' => 1], 'json', [])
            ->willReturn('{"id": 1}');

        $deserializedObject = new DeliveryInterval();
        $serializer
            ->method('deserialize')
            ->with('{"id": 1}', DeliveryInterval::class, 'json', [AbstractNormalizer::ALLOW_EXTRA_ATTRIBUTES => false])
            ->willReturn($deserializedObject);

        $hydrator = new ObjectHydratorService();
        $hydrator->addHydrator(EntityInterface::class, new EntityHydrator());

        $nameConverter = new CamelCaseToSnakeCaseNameConverter();
        $converter = new TypeConverterService($serializer, $hydrator, $nameConverter);

        $deliveryInterval = $converter->arrayToEntity($input, DeliveryInterval::class);
        $interval = $deliveryInterval->getEstimation();
        $this->assertEquals(30, $interval->i);
    }

    public function testArrayToDto(): void
    {
        $input = [
            'code' => 'restaurant_code',
            'name' => 'Restaurant Name',
        ];

        $serializer = $this->getMockBuilder(SerializerInterface::class)->getMock();
        $serializer
            ->method('serialize')
            ->with([
                'code' => 'restaurant_code',
                'name' => 'Restaurant Name',
            ], 'json', [])
            ->willReturn('{"code": "restaurant_code", "name": "Restaurant Name"}');

        $deserializedObject = new Restaurant();
        $deserializedObject->code = 'restaurant_code';
        $deserializedObject->name = 'Restaurant Name';

        $serializer
            ->method('deserialize')
            ->with(
                '{"code": "restaurant_code", "name": "Restaurant Name"}',
                Restaurant::class,
                'json',
                [AbstractNormalizer::ALLOW_EXTRA_ATTRIBUTES => false]
            )->willReturn($deserializedObject);

        $hydrator = new ObjectHydratorService();
        $hydrator->addHydrator(DtoInterface::class, new DtoHydrator());

        $nameConverter = new CamelCaseToSnakeCaseNameConverter();
        $converter = new TypeConverterService($serializer, $hydrator, $nameConverter);

        /** @var Restaurant $restaurant */
        $restaurant = $converter->arrayToDto($input, Restaurant::class);

        $this->assertEquals('restaurant_code', $restaurant->code);
        $this->assertEquals('Restaurant Name', $restaurant->name);
    }

    public function testApiResponseToJson(): void
    {
        $serializer = $this->getMockBuilder(SerializerInterface::class)->getMock();
        $serializer
            ->expects($this->once())
            ->method('serialize')
            ->with(['messages' => [], 'payload' => []], 'json', ['json_encode_options' => JsonResponse::DEFAULT_ENCODING_OPTIONS,])
            ->willReturn('{"messages": [], "payload": []}');

        $hydratorStub = $this->getMockBuilder(ObjectHydratorService::class)->setMethods([])->getMock();
        $nameConverterStub = new CamelCaseToSnakeCaseNameConverter();
        $converter = new TypeConverterService($serializer, $hydratorStub, $nameConverterStub);

        $this->assertEquals(
            '{"messages": [], "payload": []}',
            $converter->apiResponseToJson(new ApiResponse())
        );
    }
}
