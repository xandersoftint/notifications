<?php
declare(strict_types=1);

namespace App\Tests\Service;

use App\Entity\Restaurant;
use App\Exception\ObjectValidationException;
use App\Repository\RestaurantRepository;
use App\Service\RestaurantService;
use App\Service\TypeConverterService;
use PHPUnit\Framework\TestCase;
use Symfony\Component\Validator\ConstraintViolationListInterface;
use Symfony\Component\Validator\Validator\ValidatorInterface;

class RestaurantServiceTest extends TestCase
{
    private RestaurantService $service;
    private RestaurantRepository $repository;
    private ValidatorInterface $validator;
    private TypeConverterService $converter;

    protected function setUp(): void
    {
        $this->repository = $this->getMockBuilder(RestaurantRepository::class)
            ->disableOriginalConstructor()
            ->getMock();
        $this->validator = $this->getMockBuilder(ValidatorInterface::class)->setMethods([])->getMock();
        $this->converter = $this->getMockBuilder(TypeConverterService::class)->disableOriginalConstructor()->getMock();

        $this->service = new RestaurantService($this->repository);
        $this->service->setValidator($this->validator);
        $this->service->setConverter($this->converter);
        parent::setUp();
    }

    public function testCreate(): void
    {
        $input = [
            'code' => 'example_restaurant_code',
            'name' => 'Restaurant Name',
        ];

        $inputGeneratedEntity = (new Restaurant())
            ->setCode('example_restaurant_code')
            ->setName('Restaurant Name');

        $this->converter->expects($this->once())
            ->method('arrayToEntity')
            ->with($input)
            ->willReturn($inputGeneratedEntity);

        $this->repository->expects($this->once())->method('save')->with($inputGeneratedEntity)->willReturnArgument(0);

        $constraintViolationListMock = $this
            ->getMockBuilder(ConstraintViolationListInterface::class)
            ->setMethods([])
            ->getMock();
        $constraintViolationListMock->expects($this->once())->method('count')->willReturn(0);

        $this->validator
            ->expects($this->once())
            ->method('validate')
            ->with($inputGeneratedEntity)
            ->willReturn($constraintViolationListMock);


        /** @var Restaurant $entity */
        $entity = $this->service->create($input);
        $this->assertEquals('example_restaurant_code', $entity->getCode());
        $this->assertEquals('Restaurant Name', $entity->getName());
    }

    public function testCreateWithValidationException(): void
    {
        $this->expectException(ObjectValidationException::class);

        $input = [
            'code' => 'example_restaurant_code',
            'name' => 'Restaurant Name',
        ];

        $inputGeneratedEntity = (new Restaurant())
            ->setCode('example_restaurant_code')
            ->setName('Restaurant Name');

        $this->converter->expects($this->once())
            ->method('arrayToEntity')
            ->with($input)
            ->willReturn($inputGeneratedEntity);

        $constraintViolationListMock = $this
            ->getMockBuilder(ConstraintViolationListInterface::class)
            ->setMethods([])
            ->getMock();
        $constraintViolationListMock->expects($this->once())->method('count')->willReturn(1);

        $this->validator
            ->expects($this->once())
            ->method('validate')
            ->with($inputGeneratedEntity)
            ->willReturn($constraintViolationListMock);

        $this->repository->expects($this->never())->method('save');
        $this->service->create($input);
    }

    public function testGetById(): void
    {
        $this->repository->expects($this->once())->method('find')->with(1)->willReturn(new Restaurant());
        $entity = $this->service->get(1);
        $this->assertInstanceOf(Restaurant::class, $entity);
    }

    public function testGetByCode(): void
    {
        $this->repository
            ->expects($this->once())
            ->method('findByCode')
            ->with('restaurant_code')
            ->willReturn(new Restaurant());
        $entity = $this->service->getByCode('restaurant_code');
        $this->assertInstanceOf(Restaurant::class, $entity);
    }

    public function testDelete(): void
    {
        $input = new Restaurant();
        $this->repository->expects($this->once())->method('remove')->with($input);
        $this->service->delete($input);
    }

    protected function tearDown(): void
    {
        unset($this->repository, $this->service, $this->validator, $this->converter);
        parent::tearDown();
    }
}
