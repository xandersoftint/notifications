<?php
declare(strict_types=1);

namespace App\Tests\Producer;

use App\Exception\CoreException;
use App\Message\SmsMessage;
use App\MessageProducer\NotificationProducer;
use PHPUnit\Framework\TestCase;
use Symfony\Component\Messenger\MessageBusInterface;

class NotificationProducerTest extends TestCase
{
    public function testGetMessageClassByTopicWithExistingTopic()
    {
        $messageBusInterfaceStub = $this->getMockBuilder(MessageBusInterface::class)->setMethods([])->getMock();
        $producer = new NotificationProducer($messageBusInterfaceStub);

        $this->assertEquals(SmsMessage::class, $producer->getMessageClassByTopic(SmsMessage::TOPIC));
    }

    public function testGetMessageClassByTopicWithNotExistingTopic()
    {
        $this->expectException(CoreException::class);

        $messageBusInterfaceStub = $this->getMockBuilder(MessageBusInterface::class)->setMethods([])->getMock();
        $producer = new NotificationProducer($messageBusInterfaceStub);
        $producer->getMessageClassByTopic('not-existing-topic');
    }
}
