<?php
declare(strict_types=1);

namespace App\Tests\Repository;

use App\Component\AbstractCRUDRepository;
use App\Entity\Restaurant;
use App\Exception\RestaurantNotFoundException;
use App\Repository\RestaurantRepository;
use Doctrine\ORM\Persisters\Entity\EntityPersister;
use Doctrine\ORM\EntityManagerInterface;
use Doctrine\ORM\UnitOfWork;
use Doctrine\ORM\Mapping\ClassMetadata;
use PHPUnit\Framework\TestCase;
use Doctrine\Persistence\ManagerRegistry;

class RestaurantRepositoryTest extends TestCase
{
    private EntityPersister $persister;
    private EntityManagerInterface $entityManager;
    private AbstractCRUDRepository $repository;

    protected function setUp(): void
    {
        $this->persister = $this->getMockBuilder(EntityPersister::class)
            ->disableOriginalConstructor()
            ->getMock();

        $unitOfWorkMock = $this->getMockBuilder(UnitOfWork::class)
            ->disableOriginalConstructor()
            ->setMethods(['getEntityPersister'])
            ->getMock();
        $unitOfWorkMock->method('getEntityPersister')->willReturn($this->persister);

        $classMetadataStub = $this->getMockBuilder(ClassMetadata::class)
            ->disableOriginalConstructor()
            ->setMethods([])
            ->getMock();

        $this->entityManager = $this->getMockBuilder(EntityManagerInterface::class)->getMock();
        $this->entityManager->method('getClassMetadata')->willReturn($classMetadataStub);
        $this->entityManager->method('getUnitOfWork')->willReturn($unitOfWorkMock);

        $registryMock = $this->getMockBuilder(ManagerRegistry::class)->getMock();
        $registryMock->method('getManagerForClass')->willReturn($this->entityManager);

        $this->repository = new RestaurantRepository($registryMock);
        parent::setUp();
    }

    public function testFindDoesNotFindResults(): void
    {
        $this->expectException(RestaurantNotFoundException::class);

        $this->entityManager->method('find')->willReturn(null);
        $this->repository->find('12345');
    }

    public function testFindFindsResults(): void
    {
        $expected = new Restaurant();

        $this->entityManager->method('find')->with(null, 1, null, null)->willReturn($expected);
        $this->assertEquals($expected, $this->repository->find(1));
    }

    public function testFindByCodeDoesNotFindResults(): void
    {
        $this->expectException(RestaurantNotFoundException::class);

        $this->entityManager->method('find')->willReturn(null);
        $this->repository->findByCode('restaurant_code');
    }

    public function testFindByCodeFindsResults(): void
    {
        $expected = new Restaurant();

        $this->persister->method('load')->with(['code' => 'restaurant_code'])->willReturn($expected);
        $this->assertEquals($expected, $this->repository->findByCode('restaurant_code'));
    }

    protected function tearDown(): void
    {
        unset($this->persister, $this->repository, $this->entityManager);
        parent::tearDown();
    }
}
