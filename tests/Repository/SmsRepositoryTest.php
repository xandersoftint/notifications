<?php
declare(strict_types=1);

namespace App\Tests\Repository;

use App\Component\AbstractCRUDRepository;
use App\Entity\Sms;
use App\Exception\SmsNotFoundException;
use App\Repository\SmsRepository;
use Doctrine\ORM\Persisters\Entity\EntityPersister;
use Doctrine\ORM\EntityManagerInterface;
use Doctrine\ORM\UnitOfWork;
use Doctrine\ORM\Mapping\ClassMetadata;
use PHPUnit\Framework\TestCase;
use Doctrine\Persistence\ManagerRegistry;

class SmsRepositoryTest extends TestCase
{
    private EntityPersister $persister;
    private EntityManagerInterface $entityManager;
    private AbstractCRUDRepository $repository;

    protected function setUp(): void
    {
        $this->persister = $this->getMockBuilder(EntityPersister::class)
            ->disableOriginalConstructor()
            ->getMock();

        $unitOfWorkMock = $this->getMockBuilder(UnitOfWork::class)
            ->disableOriginalConstructor()
            ->setMethods(['getEntityPersister'])
            ->getMock();
        $unitOfWorkMock->method('getEntityPersister')->willReturn($this->persister);

        $classMetadataStub = $this->getMockBuilder(ClassMetadata::class)
            ->disableOriginalConstructor()
            ->setMethods([])
            ->getMock();

        $this->entityManager = $this->getMockBuilder(EntityManagerInterface::class)->getMock();
        $this->entityManager->method('getClassMetadata')->willReturn($classMetadataStub);
        $this->entityManager->method('getUnitOfWork')->willReturn($unitOfWorkMock);

        $registryMock = $this->getMockBuilder(ManagerRegistry::class)->getMock();
        $registryMock->method('getManagerForClass')->willReturn($this->entityManager);

        $this->repository = new SmsRepository($registryMock);
        parent::setUp();
    }

    public function testFindDoesNotFindResults(): void
    {
        $this->expectException(SmsNotFoundException::class);

        $this->entityManager->method('find')->willReturn(null);
        $this->repository->find('12345');
    }

    public function testFindFindsResults(): void
    {
        $expected = new Sms();

        $this->entityManager->method('find')->willReturn($expected);
        $this->assertEquals($expected, $this->repository->find('12345'));
    }

    protected function tearDown(): void
    {
        unset($this->persister, $this->repository, $this->entityManager);
        parent::tearDown();
    }
}
