<?php
declare(strict_types=1);

namespace App\Tests\Repository;

use App\Component\AbstractCRUDRepository;
use App\Entity\DeliveryInterval;
use App\Entity\DeliveryIntervalCollection;
use App\Entity\Restaurant;
use App\Exception\DeliveryIntervalNotFoundException;
use App\Repository\DeliveryIntervalRepository;
use Doctrine\ORM\Persisters\Entity\EntityPersister;
use Doctrine\ORM\EntityManagerInterface;
use Doctrine\ORM\UnitOfWork;
use Doctrine\ORM\Mapping\ClassMetadata;
use PHPUnit\Framework\TestCase;
use Doctrine\Persistence\ManagerRegistry;

class DeliveryIntervalRepositoryTest extends TestCase
{
    private EntityPersister $persister;
    private EntityManagerInterface $entityManager;
    private AbstractCRUDRepository $repository;

    protected function setUp(): void
    {
        $this->persister = $this->getMockBuilder(EntityPersister::class)
            ->disableOriginalConstructor()
            ->getMock();
        $this->persister->method('count')->willReturn(10);

        $unitOfWorkMock = $this->getMockBuilder(UnitOfWork::class)
            ->disableOriginalConstructor()
            ->setMethods(['getEntityPersister'])
            ->getMock();
        $unitOfWorkMock->method('getEntityPersister')->willReturn($this->persister);

        $classMetadataStub = $this->getMockBuilder(ClassMetadata::class)
            ->disableOriginalConstructor()
            ->setMethods([])
            ->getMock();

        $this->entityManager = $this->getMockBuilder(EntityManagerInterface::class)->getMock();
        $this->entityManager->method('getClassMetadata')->willReturn($classMetadataStub);
        $this->entityManager->method('getUnitOfWork')->willReturn($unitOfWorkMock);

        $registryMock = $this->getMockBuilder(ManagerRegistry::class)->getMock();
        $registryMock->method('getManagerForClass')->willReturn($this->entityManager);

        $this->repository = new DeliveryIntervalRepository($registryMock);
        parent::setUp();
    }

    public function testFindByRestaurantDoesNotFindResults(): void
    {
        $this->persister->method('loadAll')->willReturn([]);
        $this->assertCount(0, $this->repository->findByRestaurant(new Restaurant())->getCollection());
    }

    public function testFindByRestaurantFindsResults(): void
    {
        $expected = new DeliveryIntervalCollection([new DeliveryInterval()]);

        $this->persister->method('loadAll')->with(['restaurant' => new Restaurant()])->willReturn($expected);
        $this->assertEquals($expected, $this->repository->findByRestaurant(new Restaurant())->getCollection());
    }

    public function testFindDefaultByRestaurantFindsResult(): void
    {
        $expected = new DeliveryInterval();

        $this->persister
            ->method('load')
            ->with(['restaurant' => new Restaurant(), 'default' => true])
            ->willReturn($expected);

        $this->assertEquals($expected, $this->repository->findDefaultByRestaurant((new Restaurant())));
    }

    public function testFindDefaultByRestaurantDoesNotFindResult(): void
    {
        $this->expectException(DeliveryIntervalNotFoundException::class);

        $this->entityManager->method('find')->willReturn(null);
        $this->repository->findDefaultByRestaurant(new Restaurant());
    }

    public function testFindDoesNotFindResults(): void
    {
        $this->expectException(DeliveryIntervalNotFoundException::class);

        $this->entityManager->method('find')->willReturn(null);
        $this->repository->find('12345');
    }

    public function testFindFindsResults(): void
    {
        $expected = new DeliveryInterval();

        $this->entityManager->method('find')->willReturn($expected);
        $this->assertEquals($expected, $this->repository->find('12345'));
    }

    protected function tearDown(): void
    {
        unset($this->persister, $this->repository, $this->entityManager);
        parent::tearDown();
    }
}
