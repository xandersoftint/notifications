<?php
declare(strict_types=1);

namespace App\Tests\Dto\Integration;

use App\Dto\Integration\Response;
use App\Exception\PropertyUndefinedException;
use PHPUnit\Framework\TestCase;

class ResponseTest extends TestCase
{
    public function testGetterWithPresetValue()
    {
        $response = new Response();
        $response->contents['element'] = 'value';

        $this->assertEquals('value', $response->element);
    }

    public function testGetterWithNoPresetValue()
    {
        $this->expectException(PropertyUndefinedException::class);
        $response = new Response();
        $response->element;
    }

    public function testHeaderWithPresetValue()
    {
        $response = new Response();
        $response->headers['element'] = ['value'];

        $this->assertEquals('value', $response->getHeaderFirstValue('element'));
    }

    public function testHeaderWithNoPresetValue()
    {
        $this->expectException(PropertyUndefinedException::class);
        $response = new Response();
        $response->getHeaderFirstValue('element');
    }
}