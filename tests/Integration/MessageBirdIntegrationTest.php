<?php
declare(strict_types=1);

namespace App\Tests\Integration;

use App\Dto\Integration\Request;
use App\Dto\Integration\Response;
use App\Service\EnvironmentService;
use App\Service\IntegrationService;
use App\Integration\MessageBirdNotification;
use PHPUnit\Framework\TestCase;

class MessageBirdIntegrationTest extends TestCase
{
    public function testSend(): void
    {
        $environment = new EnvironmentService('/', 'en');
        $environment->setSms('https://rest.messagebird.com', '123456');
        $integration = $this->getMockBuilder(IntegrationService::class)
            ->disableOriginalConstructor()
            ->setMethods([])
            ->getMock();
        $integration->expects($this->once())->method('execute')->willReturnCallback(function (Request $request) {
            $this->assertEquals([
                'headers' => [
                    'Authorization' => 'AccessKey 123456',
                    'Accept-Language' => 'en'
                ],
                'body' => [
                    'recipients' => ['+40722222222'],
                    'originator' => 'Hard Rock Cafe',
                    'body' => 'Hard Rock Cafe will deliver your meal in 30 minutes',
                ]
            ], $request->convertToHttpClientOptions());

            return new Response();
        });

        $service = new MessageBirdNotification($environment, $integration);

        $service->send(['+40722222222'], 'Hard Rock Cafe', 'Hard Rock Cafe will deliver your meal in 30 minutes');
    }
}
