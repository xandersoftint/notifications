<?php
declare(strict_types=1);

namespace App\Tests\MessageHandler;

use App\Entity\Sms;
use App\Facade\NotificationFacadeInterface;
use App\Message\SmsMessage;
use App\MessageHandler\NotificationHandler;
use App\Service\TypeConverterService;
use PHPUnit\Framework\TestCase;
use Psr\Log\LoggerInterface;

class NotificationHandlerTest extends TestCase
{
    public function testInvokeMethod()
    {
        $facade = $this->getMockBuilder(NotificationFacadeInterface::class)->setMethods([
            'get',
            'send',
            'envelopeAndSend',
            'generate',
            'create'
        ])->getMock();

        $facade->expects($this->once())->method('get')->with(123)->willReturn(new Sms());
        $facade->expects($this->once())->method('send')->with(new Sms());

        $converter = $this->getMockBuilder(TypeConverterService::class)
            ->disableOriginalConstructor()
            ->setMethods([])
            ->getMock();
        $logger = $this->getMockBuilder(LoggerInterface::class)->setMethods([])->getMock();

        $handler = new NotificationHandler($facade);
        $handler->setConverter($converter);
        $handler->setLogger($logger);

        $handler(new SmsMessage(123));
    }
}
