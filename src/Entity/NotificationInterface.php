<?php
declare(strict_types=1);

namespace App\Entity;

interface NotificationInterface extends EntityInterface
{
    public const STATUS_NEW = 'new';
    public const STATUS_SENT = 'sent';
    public const STATUS_ERROR = 'error';
}
