<?php
declare(strict_types=1);

namespace App\Entity;

use App\Component\AbstractCollection;

class EntityCollection extends AbstractCollection
{
    protected function isTypeSupported($value): bool
    {
        return $value instanceof EntityInterface;
    }
}
