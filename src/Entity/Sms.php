<?php
declare(strict_types=1);

namespace App\Entity;

use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Validator\Constraints as Assert;

/**
 * @ORM\HasLifecycleCallbacks()
 * @ORM\Table(name="sms", indexes={
 *     @ORM\Index(name="search_status_idx", columns={"status"})
 * }))
 * @ORM\Entity(repositoryClass="App\Repository\SmsRepository")
 */
class Sms implements NotificationInterface
{
    /**
     * @ORM\Id()
     * @ORM\GeneratedValue(strategy="AUTO")
     * @ORM\Column(type="integer")
     */
    private ?int $id = 0;

    /**
     * @ORM\Column(type="string", length=8, nullable=false)
     * @Assert\Choice(
     *     choices={
     *      NotificationInterface::STATUS_NEW,
     *      NotificationInterface::STATUS_SENT,
     *      NotificationInterface::STATUS_ERROR
     *     },
     *     message="Status does not contain a valid value"
     * )
     */
    private string $status = self::STATUS_NEW;

    /**
     * @ORM\Column(name="source", type="string", length=32, nullable=false)
     * @Assert\NotBlank(message="Specify from whom you send")
     * @Assert\Length(max="11", maxMessage="From could not be longer than 11 chars")
     * @Assert\Regex(pattern="/^[a-zA-Z0-9]+$/", message="From value should be alphanumeric")
     */
    private string $from;

    /**
     * @ORM\Column(type="string", length=16)
     * @Assert\NotBlank(message="The destination should be populated")
     * @Assert\Length(
     *     min=12,
     *     minMessage="Destination should contain a minimum of 12 chars",
     *     max="15",
     *     maxMessage="Destination should contain a maximum of 15 chars"
     * )
     * @Assert\Regex(
     *     pattern="/^\+(?:[0-9]*?){6,14}[0-9]$/",
     *     message="Destination is not a valid international phone number"
     * )
     */
    private string $destination;

    /**
     * @ORM\Column(type="string", length=256, nullable=false)
     * @Assert\NotBlank(message="Sms should contain a message")
     */
    private string $message;

    /**
     * @ORM\Column(
     *     type="datetime",
     *     nullable=false,
     *     name="created_at",
     *     options={"default": "CURRENT_TIMESTAMP"},
     * )
     */
    private \DateTime $createdAt;

    public function getId(): int
    {
        return $this->id;
    }

    /** @ORM\PreFlush() */
    public function setCreatedAt(): void
    {
        if (isset($this->createdAt)) {
            return;
        }

        $this->createdAt = new \DateTime();
    }

    public function getCreatedAt(): \DateTime
    {
        return $this->createdAt;
    }

    public function getStatus(): string
    {
        return $this->status;
    }

    public function setStatus(string $status): Sms
    {
        $this->status = $status;
        return $this;
    }

    public function getDestination(): string
    {
        return $this->destination;
    }

    public function setDestination(string $destination): Sms
    {
        $this->destination = $destination;
        return $this;
    }

    public function getMessage(): string
    {
        return $this->message;
    }

    public function setMessage(string $message): Sms
    {
        $this->message = $message;
        return $this;
    }

    public function getFrom(): string
    {
        return $this->from;
    }

    public function setFrom(string $from): Sms
    {
        $this->from = $from;
        return $this;
    }
}
