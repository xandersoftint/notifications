<?php
declare(strict_types=1);
namespace App\Entity;

use App\Exception\PropertyUndefinedException;
use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Validator\Constraints as Assert;

/**
 * @ORM\HasLifecycleCallbacks()
 * @ORM\Table(name="delivery_interval", indexes={
 *     @ORM\Index(name="search_idx", columns={"restaurant_id"})
 * }))
 * @ORM\Entity(repositoryClass="App\Repository\DeliveryIntervalRepository")
 */
class DeliveryInterval implements EntityInterface
{
    /**
     * @ORM\Id()
     * @ORM\GeneratedValue(strategy="AUTO")
     * @ORM\Column(type="integer")
     */
    private ?int $id = 0;

    /**
     * @ORM\ManyToOne(targetEntity="Restaurant")
     * @ORM\JoinColumn(name="restaurant_id", referencedColumnName="id")
     */
    private Restaurant $restaurant;

    /**
     * @ORM\Column(type="dateinterval")
     * @Assert\NotBlank(message="Delivery estimation time should be populated")
     */
    private \DateInterval $estimation;

    /**
     * @ORM\Column(name="is_default", type="boolean", options={"default":0}, nullable=false)
     */
    private bool $default = false;

    /**
     * @ORM\Column(
     *     type="datetime",
     *     name="begin_at",
     *     nullable=true
     * )
     */
    private \DateTime $beginAt;

    /**
     * @ORM\Column(
     *     name="end_at",
     *     type="datetime",
     *     nullable=true
     * )
     */
    private \DateTime $endAt;

    /**
     * @ORM\Column(
     *     type="datetime",
     *     nullable=false,
     *     name="created_at",
     *     options={"default": "CURRENT_TIMESTAMP"},
     * )
     */
    private \DateTime $createdAt;

    /**
     * @ORM\Column(
     *     name="updated_at",
     *     type="datetime",
     *     nullable=false,
     *     columnDefinition="DATETIME NOT NULL on update CURRENT_TIMESTAMP DEFAULT CURRENT_TIMESTAMP"
     * )
     */
    private \DateTime $updatedAt;

    public function __construct()
    {
        $this->beginAt = \DateTime::createFromFormat('!H:i', '00:00');
        $this->endAt = \DateTime::createFromFormat('!H:i', '00:00');
    }

    public function getId(): int
    {
        return $this->id;
    }

    public function setEstimation(\DateInterval $interval): DeliveryInterval
    {
        $this->estimation = $interval;
        return $this;
    }

    public function getEstimation(): \DateInterval
    {
        return $this->estimation;
    }

    public function getRestaurant(): Restaurant
    {
        return $this->restaurant;
    }

    public function setRestaurant(Restaurant $restaurant): DeliveryInterval
    {
        $this->restaurant = $restaurant;
        return $this;
    }

    /** @ORM\PreFlush() */
    public function setCreatedAt(): void
    {
        if (isset($this->createdAt)) {
            return;
        }

        $this->createdAt = new \DateTime();
    }

    public function getCreatedAt(): \DateTime
    {
        return $this->createdAt;
    }

    /** @ORM\PrePersist */
    public function setUpdatedAt(): void
    {
        $this->updatedAt = new \DateTime();
    }

    public function getUpdatedAt(): \DateTime
    {
        return $this->updatedAt;
    }

    public function getBeginAt(): \DateTime
    {
        return $this->beginAt;
    }

    public function setBeginAt(\DateTime $beginAt): DeliveryInterval
    {
        $this->beginAt = $beginAt;
        return $this;
    }

    public function getEndAt(): \DateTime
    {
        if (!isset($this->endAt)) {
            throw new PropertyUndefinedException();
        }

        return $this->endAt;
    }

    public function setEndAt(\DateTime $endAt): DeliveryInterval
    {
        $this->endAt = $endAt;
        return $this;
    }

    public function isDefault(): bool
    {
        return $this->default;
    }

    public function setDefault(bool $default): DeliveryInterval
    {
        $this->default = $default;
        return $this;
    }
}
