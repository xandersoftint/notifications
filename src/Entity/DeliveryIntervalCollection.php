<?php
declare(strict_types=1);

namespace App\Entity;

class DeliveryIntervalCollection extends EntityCollection
{
    protected function isTypeSupported($value): bool
    {
        return $value instanceof DeliveryInterval;
    }
}
