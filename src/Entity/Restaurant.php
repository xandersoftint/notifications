<?php
declare(strict_types=1);

namespace App\Entity;

use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Validator\Constraints as Assert;
use Symfony\Bridge\Doctrine\Validator\Constraints as ORMAssert;

/**
 * @ORM\HasLifecycleCallbacks()
 * @ORM\Table(name="restaurant", indexes={
 *     @ORM\Index(name="search_idx", columns={"code"})
 * }))
 * @ORM\Entity(repositoryClass="App\Repository\RestaurantRepository")
 *
 * @ORMAssert\UniqueEntity(fields={"code"}, message="The Restaurant code is already in use")
 */
class Restaurant implements EntityInterface
{
    /**
     * @ORM\Id()
     * @ORM\GeneratedValue(strategy="AUTO")
     * @ORM\Column(type="integer")
     */
    private ?int $id = 0;

    /**
     * @ORM\Column(type="string", length=32, unique=true)
     * @Assert\NotBlank(message="Restaurant code identifier should be populated")
     */
    private string $code;

    /**
     * @ORM\Column(type="string", length=64, nullable=false)
     * @Assert\NotBlank(message="Restaurant name should be populated")
     */
    private string $name;

    /**
     * @ORM\OneToMany(
     *     targetEntity="App\Entity\DeliveryInterval",
     *     mappedBy="restaurant",
     *     fetch="EXTRA_LAZY",
     *     cascade={"remove"}
     *)
     */
    private Collection $intervals;

    /**
     * @ORM\Column(
     *     type="datetime",
     *     nullable=false,
     *     name="created_at",
     *     options={"default": "CURRENT_TIMESTAMP"},
     * )
     */
    private \DateTime $createdAt;

    /**
     * @ORM\Column(
     *     name="updated_at",
     *     type="datetime",
     *     nullable=false,
     *     columnDefinition="DATETIME NOT NULL on update CURRENT_TIMESTAMP DEFAULT CURRENT_TIMESTAMP"
     * )
     */
    private \DateTime $updatedAt;

    public function __construct()
    {
        $this->intervals = new ArrayCollection();
    }

    public function getId(): int
    {
        return $this->id;
    }

    /** @ORM\PreFlush() */
    public function setCreatedAt(): void
    {
        if (isset($this->createdAt)) {
            return;
        }

        $this->createdAt = new \DateTime();
    }

    public function getCreatedAt(): \DateTime
    {
        return $this->createdAt;
    }

    /** @ORM\PrePersist */
    public function setUpdatedAt(): void
    {
        $this->updatedAt = new \DateTime();
    }

    public function getUpdatedAt(): \DateTime
    {
        return $this->updatedAt;
    }

    public function getCode(): string
    {
        return $this->code;
    }

    public function setCode(string $code): Restaurant
    {
        $this->code = $code;
        return $this;
    }

    public function getName(): string
    {
        return $this->name;
    }

    public function setName(string $name): Restaurant
    {
        $this->name = $name;
        return $this;
    }
}
