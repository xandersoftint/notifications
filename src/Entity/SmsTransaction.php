<?php
declare(strict_types=1);

namespace App\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\HasLifecycleCallbacks()
 * @ORM\Table(name="sms_transaction", indexes={
 *     @ORM\Index(name="search_idx", columns={"notification_id"})
 * }))
 * @ORM\Entity(repositoryClass="App\Repository\SmsTransactionRepository")
 */
class SmsTransaction implements EntityInterface
{
    /**
     * @ORM\Id()
     * @ORM\GeneratedValue(strategy="AUTO")
     * @ORM\Column(type="integer")
     */
    private ?int $id = 0;

    /**
     * @ORM\ManyToOne(targetEntity="Sms")
     * @ORM\JoinColumn(name="notification_id", referencedColumnName="id")
     */
    private Sms $sms;

    /**
     * @ORM\Column(type="integer", nullable=false)
     */
    private int $status;

    /**
     * @ORM\Column(type="json", nullable=true)
     */
    private array $response;

    /**
     * @ORM\Column(
     *     type="datetime",
     *     nullable=false,
     *     name="created_at",
     *     options={"default": "CURRENT_TIMESTAMP"},
     * )
     */
    private \DateTime $createdAt;

    public function getId(): int
    {
        return $this->id;
    }

    /** @ORM\PreFlush() */
    public function setCreatedAt(): void
    {
        if (isset($this->createdAt)) {
            return;
        }

        $this->createdAt = new \DateTime();
    }

    public function getCreatedAt(): \DateTime
    {
        return $this->createdAt;
    }

    public function getSms(): Sms
    {
        return $this->sms;
    }

    public function setSms(Sms $sms): SmsTransaction
    {
        $this->sms = $sms;
        return $this;
    }

    public function getStatus(): int
    {
        return $this->status;
    }

    public function setStatus(int $status): SmsTransaction
    {
        $this->status = $status;
        return $this;
    }

    public function getResponse(): array
    {
        return $this->response;
    }

    public function setResponse(array $response): SmsTransaction
    {
        $this->response = $response;
        return $this;
    }
}
