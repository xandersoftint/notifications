<?php
declare(strict_types=1);

namespace App\Entity;

use App\Component\AbstractCollection;

class PaginatedCollection
{
    protected int $total;
    protected int $filtered;
    protected AbstractCollection $collection;

    public function __construct(AbstractCollection $collection, int $total, int $filtered)
    {
        $this->collection = $collection;
        $this->total = $total;
        $this->filtered = $filtered;
    }

    public function getTotal(): int
    {
        return $this->total;
    }

    public function getFiltered(): int
    {
        return $this->filtered;
    }

    public function getCollection(): AbstractCollection
    {
        return $this->collection;
    }
}
