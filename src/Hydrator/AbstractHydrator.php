<?php
declare(strict_types=1);

namespace App\Hydrator;

use Psr\Log\LoggerInterface;

abstract class AbstractHydrator
{
    protected LoggerInterface $logger;

    /** @required */
    public function setLogger(LoggerInterface $logger): void
    {
        $this->logger = $logger;
    }

    abstract public function hydrate($object, array $data);
}
