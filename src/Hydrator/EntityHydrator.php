<?php
declare(strict_types=1);

namespace App\Hydrator;

use App\Entity\EntityInterface;

class EntityHydrator extends AbstractHydrator
{
    public function hydrate($entity, array $dataSet): EntityInterface
    {
        foreach ($dataSet as $key => $element) {
            try {
                $method = "set{$key}";
                $entity->$method($element);
            } catch (\Throwable $e) {
                $this->logger->notice($e);
            }
        }

        return $entity;
    }
}
