<?php
declare(strict_types=1);

namespace App\Hydrator;

use App\Dto\DtoInterface;
use App\Exception\ConversionException;
use App\Exception\CoreException;
use App\Exception\PropertyUndefinedException;

class DtoHydrator extends AbstractHydrator
{
    public function hydrate($dto, array $dataSet): DtoInterface
    {
        foreach ($dataSet as $key => $element) {
            $mutatorName = "mutate{$key}";
            try {
                $dto->$key = $dto->$mutatorName($element);
                continue;
            } catch (ConversionException $e) {
                throw $e;
            } catch (\TypeError $e) {
                $this->logger->notice($e);
            } catch (\Error $e) {
                //Do nothing, mutator does not exist
            }

            try {
                if (!\property_exists($dto, $key)) {
                    throw (new PropertyUndefinedException())->setContext([
                        'class' => \get_class($dto),
                        'key' => $key,
                    ]);
                }
                $dto->$key = $element;
            } catch (CoreException $e) {
                $this->logger->notice($e, $e->getContext());
            } catch (\Throwable $e) {
                $this->logger->notice($e);
            }
        }

        return $dto;
    }
}
