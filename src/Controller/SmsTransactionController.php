<?php
declare(strict_types=1);

namespace App\Controller;

use App\Facade\SmsFacade;
use App\Service\SmsTransactionService;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;

class SmsTransactionController extends AbstractController
{
    private SmsTransactionService $service;

    /** @required */
    public function setService(SmsTransactionService $service)
    {
        $this->service = $service;
    }

    public function listErrors(Request $request, int $days): Response
    {
        return $this->json($this->service->listErrors($days));
    }

    public function list(Request $request, int $days): Response
    {
        $offset = (int)$request->get('start', 0);
        $limit = (int)$request->get('length', 50);
        $search = $request->get('search', ['value' => ''])['value'] ?? '';
        return $this->json($this->service->list($days, $offset, $limit, $search));
    }
}
