<?php
declare(strict_types=1);

namespace App\Controller;

use App\Service\OpenApiService;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;

class OpenApiController extends AbstractController
{
    private OpenApiService $openApiService;

    public function __construct(OpenApiService $openApiService)
    {
        $this->openApiService = $openApiService;
    }

    public function read(Request $request, string $version): JsonResponse
    {
        return new JsonResponse($this->openApiService->extractDocumentationFromVersionFile($version));
    }
}
