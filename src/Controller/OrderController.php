<?php
declare(strict_types=1);

namespace App\Controller;

use App\Facade\OrderFacade;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;

class OrderController extends AbstractController
{
    private OrderFacade $facade;

    /** @required */
    public function setFacade(OrderFacade $facade): void
    {
        $this->facade = $facade;
    }

    public function sendConfirmationNotification(Request $request): Response
    {
        $this->facade->sendNotification($this->facade->generateConfirmationNotification($request->getContent()));
        return $this->json([]);
    }

    public function sendReviewNotification(Request $request): Response
    {
        $this->facade->sendNotification($this->facade->generateReviewNotification($request->getContent()));
        return $this->json([]);
    }

    public function sendOrderNotification(Request $request): Response
    {
        $this->facade->sendOrderNotification($this->facade->generateConfirmationNotification($request->getContent()));
        return $this->json([]);
    }
}
