<?php
declare(strict_types=1);

namespace App\Controller;

use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;

interface CRUDControllerInterface
{
    public function create(Request $request): Response;
    public function read(int $id): Response;
    public function update(Request $request, int $id): Response;
    public function delete(int $id): Response;
}
