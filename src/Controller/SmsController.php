<?php
declare(strict_types=1);

namespace App\Controller;

use App\Facade\SmsFacade;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;

class SmsController extends AbstractController implements CRUDControllerInterface
{
    private SmsFacade $facade;

    /** @required */
    public function setFacade(SmsFacade $facade)
    {
        $this->facade = $facade;
    }

    public function create(Request $request): Response
    {
        return $this->json($this->facade->create($this->facade->generate($request->getContent())));
    }

    public function read(int $id): Response
    {
        return $this->json($this->facade->get($id));
    }

    public function update(Request $request, int $id): Response
    {
        // TODO: Implement update() method.
    }

    public function delete(int $id): Response
    {
        $entity = $this->facade->get($id);
        $this->facade->delete($entity);
        return $this->json([]);
    }

    public function send(int $id): Response
    {
        return $this->json($this->facade->send($this->facade->get($id)));
    }
}
