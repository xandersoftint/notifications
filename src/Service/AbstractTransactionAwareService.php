<?php
declare(strict_types=1);

namespace App\Service;

use App\Component\TransactionAwareTrait;

abstract class AbstractTransactionAwareService extends AbstractService
{
    use TransactionAwareTrait;
}
