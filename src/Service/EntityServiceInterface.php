<?php
declare(strict_types=1);

namespace App\Service;

use App\Entity\EntityInterface;

interface EntityServiceInterface
{
    public function create(array $data): EntityInterface;
    public function update(EntityInterface $entity, array $data = []): EntityInterface;
    public function get(int $id): EntityInterface;
    public function delete(EntityInterface $entity): void;
}
