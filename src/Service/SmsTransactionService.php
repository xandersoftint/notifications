<?php
declare(strict_types=1);

namespace App\Service;

use App\Entity\EntityInterface;
use App\Entity\PaginatedCollection;
use App\Entity\SmsTransaction;
use App\Repository\SmsTransactionRepository;

class SmsTransactionService extends AbstractTransactionAwareService implements EntityServiceInterface
{
    public function __construct(SmsTransactionRepository $repository)
    {
        $this->setRepository($repository);
    }

    public function create(array $data): EntityInterface
    {
        $entity = $this->converter->arrayToEntity($data, SmsTransaction::class);
        $this->validate($entity);

        return $this->repository->save($entity);
    }

    public function update(EntityInterface $entity, array $data = []): EntityInterface
    {
        // TODO: Implement update() method.
        return $entity;
    }

    public function get(int $id): EntityInterface
    {
        return $this->repository->find($id);
    }

    public function delete(EntityInterface $entity): void
    {
        $this->repository->remove($entity);
    }

    public function listErrors(int $days, \DateTime $custom = null): PaginatedCollection
    {
        $custom = $custom ?? new \DateTime();
        $lastDays = $custom->sub(new \DateInterval("P{$days}D"));

        return $this->repository->listErrorsBy($lastDays);
    }

    public function list(int $days, int $offset, int $limit, string $search, \DateTime $custom = null): PaginatedCollection
    {
        $custom = $custom ?? new \DateTime();
        $lastDays = $custom->sub(new \DateInterval("P{$days}D"));

        return $this->repository->listBy($lastDays, $offset, $limit, $search);
    }
}
