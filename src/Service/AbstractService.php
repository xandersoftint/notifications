<?php
declare(strict_types=1);

namespace App\Service;

use App\Component\ValidatorAwareTrait;
use Symfony\Component\Validator\Validator\ValidatorInterface;

abstract class AbstractService
{
    use ValidatorAwareTrait;

    protected TypeConverterService $converter;
    protected EnvironmentService $environmentService;

    /** @required */
    public function setValidator(ValidatorInterface $validator): void
    {
        $this->validator = $validator;
    }

    /** @required */
    public function setConverter(TypeConverterService $converter): void
    {
        $this->converter = $converter;
    }

    /** @required */
    public function setEnvironmentService(EnvironmentService $service): void
    {
        $this->environmentService = $service;
    }
}
