<?php
declare(strict_types=1);

namespace App\Service;

use App\Entity\EntityInterface;
use App\Entity\Restaurant;
use App\Repository\RestaurantRepository;

class RestaurantService extends AbstractTransactionAwareService implements EntityServiceInterface
{
    public function __construct(RestaurantRepository $repository)
    {
        $this->setRepository($repository);
    }

    public function create(array $data): EntityInterface
    {
        $entity = $this->converter->arrayToEntity($data, Restaurant::class);
        $this->validate($entity);

        return $this->repository->save($entity);
    }

    public function update(EntityInterface $entity, array $data = []): EntityInterface
    {
        // TODO: Implement update() method.
        return $entity;
    }

    public function get(int $id): EntityInterface
    {
        return $this->repository->find($id);
    }

    public function getByCode(string $code): EntityInterface
    {
        return $this->repository->findByCode($code);
    }

    public function delete(EntityInterface $entity): void
    {
        $this->repository->remove($entity);
    }
}
