<?php
declare(strict_types=1);

namespace App\Service;

use App\Dto\Integration\Connection;
use App\Dto\Integration\Request as IntegrationRequest;

class EnvironmentService
{
    private string $projectDir;
    private string $defaultLocale;
    private string $locale;
    private int $configDelayOrderReviewNotificationSeconds = 0;

    private Connection $sms;

    public function __construct(string $projectDir, string $defaultLocale)
    {
        $this->projectDir = $projectDir;
        $this->defaultLocale = $defaultLocale;
        IntegrationRequest::$defaultApiLanguage = $defaultLocale;
    }

    /** @required */
    public function setSms(string $smsHost, string $smsKey): void
    {
        $this->sms = new Connection();
        $this->sms->host = $smsHost;
        $this->sms->key = $smsKey;
    }

    /** @required */
    public function setConfig(int $configDelayOrderReviewNotificationSeconds): void
    {
        $this->configDelayOrderReviewNotificationSeconds = $configDelayOrderReviewNotificationSeconds;
    }

    public function setLocale(string $locale): EnvironmentService
    {
        $this->locale = $locale;
        return $this;
    }

    public function getLocale(): string
    {
        return $this->locale ?? $this->defaultLocale;
    }

    public function getProjectDir(): string
    {
        return $this->projectDir;
    }

    public function getSms(): Connection
    {
        return $this->sms;
    }

    public function getConfigDelayOrderReviewNotificationSeconds(): int
    {
        return $this->configDelayOrderReviewNotificationSeconds;
    }
}
