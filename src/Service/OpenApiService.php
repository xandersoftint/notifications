<?php
declare(strict_types=1);

namespace App\Service;

use App\Component\StringAlterationTrait;
use App\Exception\CoreException;
use Symfony\Component\Yaml\Yaml;

class OpenApiService extends AbstractService
{
    use StringAlterationTrait;

    protected const RELATIVE_OPEN_API_SOURCE_PATH = "/openapi/{version}.yaml";

    public function extractDocumentationFromVersionFile(string $version): array
    {
        $projectDir = $this->environmentService->getProjectDir();
        $documentationPath = $projectDir . $this->hydrateUri(
            static::RELATIVE_OPEN_API_SOURCE_PATH,
            ["version" => "{$version}"]
        );

        if (!\file_exists($documentationPath)) {
            throw new CoreException('Documentation not found.');
        }

        return Yaml::parseFile($documentationPath);
    }
}
