<?php
declare(strict_types=1);

namespace App\Service;

use App\Component\HydratorAwareTrait;
use App\Entity\EntityInterface;
use App\Entity\Sms;
use App\Repository\SmsRepository;

class SmsService extends AbstractTransactionAwareService implements EntityServiceInterface
{
    use HydratorAwareTrait;

    public function __construct(SmsRepository $repository)
    {
        $this->setRepository($repository);
    }

    /** @required */
    public function setHydrator(ObjectHydratorService $hydrator): void
    {
        $this->hydrator = $hydrator;
    }

    public function create(array $data): EntityInterface
    {
        $entity = $this->converter->arrayToEntity($data, Sms::class);
        $this->validate($entity);

        return $this->repository->save($entity);
    }

    public function update(EntityInterface $entity, array $data = []): EntityInterface
    {
        if ($data) {
            $entity = $this->hydrator->hydrate($entity, $data);
            $this->validate($entity);
        }

        return $this->repository->save($entity);
    }

    public function get(int $id): EntityInterface
    {
        return $this->repository->find($id);
    }

    public function delete(EntityInterface $entity): void
    {
        $this->repository->remove($entity);
    }
}
