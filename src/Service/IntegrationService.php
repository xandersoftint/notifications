<?php
declare(strict_types=1);

namespace App\Service;

use App\Dto\Integration\Request;
use App\Dto\Integration\Response;
use App\Exception\IntegrationRespondedException;
use Psr\Log\LoggerAwareTrait;
use Psr\Log\LoggerInterface;
use Symfony\Component\HttpClient\Exception\ClientException;
use Symfony\Contracts\HttpClient\HttpClientInterface;
use App\Exception\IntegrationException;

class IntegrationService
{
    use LoggerAwareTrait;

    private HttpClientInterface $client;
    private TypeConverterService $converter;

    public function __construct(
        HttpClientInterface $client,
        LoggerInterface $logger,
        TypeConverterService $converter
    ) {
        $this->client = $client;
        $this->converter = $converter;
        $this->setLogger($logger);
    }

    public function execute(Request $request): Response
    {
        $this->logger->info('Sending integration request: ', [
            'URI' => $request->getUri(),
            'Method' => $request->getMethod(),
            'Content' => $request->convertToHttpClientOptions(),
        ]);

        try {
            $response = $this->client->request(
                $request->getMethod(),
                $request->getUri(),
                $request->convertToHttpClientOptions()
            );

            $content = $response->getContent(); //runs request
        } catch (ClientException $e) {
            $response = $e->getResponse();
            $content = $response->getContent(false);

            $this->logger->warning('Receiving integration response: ', [
                'Source' => $request->getUri(),
                'Status' => $response->getStatusCode(),
                'Headers' => $response->getHeaders(false),
                'Content' => $content,
            ]);
            throw (new IntegrationRespondedException($e->getMessage(), $e->getCode(), $e))
                ->setResponse($this->hydrateResponse(new Response(), $content));
        } catch (\Throwable $e) {
            throw new IntegrationException($e->getMessage(), $e->getCode(), $e);
        }

        $this->logger->info('Receiving integration response: ', [
            'Source' => $request->getUri(),
            'Status' => $response->getStatusCode(),
            'Headers' => $response->getHeaders(false),
            'Content' => $content,
        ]);

        return $this->hydrateResponse(new Response(), $content, $response->getHeaders(false));
    }

    private function hydrateResponse(Response $response, string $content, array $headers = []): Response
    {
        $response->contents = $this->converter->jsonToArray($content);
        $response->headers = $headers;
        return $response;
    }
}
