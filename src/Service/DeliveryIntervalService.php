<?php
declare(strict_types=1);

namespace App\Service;

use App\Entity\DeliveryInterval;
use App\Entity\EntityInterface;
use App\Entity\Restaurant;
use App\Exception\DeliveryIntervalNotFoundException;
use App\Repository\DeliveryIntervalRepository;

class DeliveryIntervalService extends AbstractTransactionAwareService implements EntityServiceInterface
{
    public function __construct(DeliveryIntervalRepository $repository)
    {
        $this->setRepository($repository);
    }

    public function create(array $data): EntityInterface
    {
        $entity = $this->converter->arrayToEntity($data, DeliveryInterval::class);
        $this->validate($entity);

        return $this->repository->save($entity);
    }

    public function update(EntityInterface $entity, array $data = []): EntityInterface
    {
        // TODO: Implement update() method.
        return $entity;
    }

    public function get(int $id): EntityInterface
    {
        return $this->repository->find($id);
    }

    public function delete(EntityInterface $entity): void
    {
        $this->repository->remove($entity);
    }

    public function getByRestaurantAndTime(Restaurant $restaurant, \DateTime $time): DeliveryInterval
    {
        try {
            return $this->repository->findByRestaurantAndTime($restaurant, $time);
        } catch (DeliveryIntervalNotFoundException $e) {
            return $this->repository->findDefaultByRestaurant($restaurant);
        }
    }
}
