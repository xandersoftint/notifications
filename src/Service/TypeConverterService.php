<?php
declare(strict_types=1);

namespace App\Service;

use App\Component\HydratorAwareTrait;
use App\Dto\ApiResponse;
use App\Dto\DtoInterface;
use App\Entity\EntityInterface;
use App\Exception\ConversionException;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\Serializer\NameConverter\NameConverterInterface;
use Symfony\Component\Serializer\Normalizer\AbstractNormalizer;
use Symfony\Component\Serializer\SerializerAwareTrait;
use Symfony\Component\Serializer\SerializerInterface;

class TypeConverterService
{
    use SerializerAwareTrait, HydratorAwareTrait;

    private NameConverterInterface $nameConverter;

    public function __construct(
        SerializerInterface $serializer,
        ObjectHydratorService $objectHydrator,
        NameConverterInterface $nameConverter
    ) {
        $this->setSerializer($serializer);
        $this->setHydrator($objectHydrator);
        $this->nameConverter = $nameConverter;
    }

    public function arrayToJson(array $data, array $context = []): string
    {
        return $this->serializer->serialize($data, 'json', $context);
    }

    public function jsonToArray(string $data): array
    {
        return $this->denormalizeArrayKeys(\json_decode($data, true) ?? []);
    }

    public function apiResponseToJson(ApiResponse $response): string
    {
        return $this->serializer->serialize(
            $this->normalizeArrayKeys($response->toArray()),
            'json',
            ['json_encode_options' => JsonResponse::DEFAULT_ENCODING_OPTIONS,]
        );
    }

    public function arrayToEntity(array $data, string $toEntityType, array $context = []): EntityInterface
    {
        return $this->arrayToObject($data, $toEntityType, $context);
    }

    public function arrayToDto(array $data, string $toDtoType, array $context = []): DtoInterface
    {
        return $this->hydrator->hydrate(new $toDtoType(), $data);
    }

    public function objectToArray(object $object): array
    {
        return $this->jsonToArray($this->serializer->serialize($object, 'json'));
    }

    private function arrayToObject(array $data, string $toObjectType, array $context = [])
    {
        $nativeData = [];
        $objectSet = [];

        foreach ($data as $key => $information) {
            if (\is_object($information)) {
                $objectSet[$key] = $information;
                continue;
            }

            $nativeData[$key] = $information;
        }

        try {
            $object = $this->serializer->deserialize(
                $this->arrayToJson($nativeData),
                $toObjectType,
                'json',
                \array_merge([
                    AbstractNormalizer::ALLOW_EXTRA_ATTRIBUTES => false,
                ], $context)
            );
            return $this->hydrator->hydrate($object, $objectSet);
        } catch (\Throwable $e) {
            throw new ConversionException($e->getMessage(), $e->getCode(), $e);
        }
    }

    private function denormalizeArrayKeys(array $data): array
    {
        $convertedData = [];
        foreach ($data as $key => $value) {
            if (\is_iterable($value)) {
                $value = $this->denormalizeArrayKeys($value);
            }
            $convertedData[$this->nameConverter->denormalize((string)$key)] = $value;
        }

        return $convertedData;
    }

    private function normalizeArrayKeys(array $data): array
    {
        $convertedData = [];
        foreach ($data as $key => $value) {
            if (\is_iterable($value)) {
                $value = $this->normalizeArrayKeys($value);
            }
            $convertedData[$this->nameConverter->normalize((string)$key)] = $value;
        }

        return $convertedData;
    }
}
