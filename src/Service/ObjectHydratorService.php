<?php
declare(strict_types=1);

namespace App\Service;

use App\Dto\DtoInterface;
use App\Entity\EntityInterface;
use App\Exception\CoreException;
use App\Hydrator\AbstractHydrator;

class ObjectHydratorService
{
    protected array $hydrators = [];

    public function addHydrator(string $key, AbstractHydrator $hydrator): void
    {
        $this->hydrators[$key] = $hydrator;
    }

    public function hydrate($object, array $data)
    {
        switch (true) {
            case $object instanceof EntityInterface:
                return $this->hydrators[EntityInterface::class]->hydrate($object, $data);
            case $object instanceof DtoInterface:
                return $this->hydrators[DtoInterface::class]->hydrate($object, $data);
            default:
                throw (new CoreException("Hydrator not configured properly for object"))->setContext([
                    'object' => \get_class($object)
                ]);
        }
    }
}
