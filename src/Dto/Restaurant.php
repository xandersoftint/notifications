<?php
declare(strict_types=1);

namespace App\Dto;

/**
 * @property string $code
 * @property string $name
 * @property array<DateInterval> $intervals
 */
class Restaurant implements DtoInterface
{
    public string $code;
    public string $name;
    public array $intervals;

    public function toArray(): array
    {
        $result = [];
        foreach (['code', 'name'] as $key) {
            if (!isset($this->$key)) {
                continue;
            }
            $result[$key] = $this->$key;
        }

        return $result;
    }
}
