<?php
declare(strict_types=1);

namespace App\Dto;

use App\Entity\EntityInterface;
use App\Exception\ConversionException;

/**
 * @property \DateTime $beginAt
 * @property \DateTime $endAt
 * @property \DateInterval $estimation
 */
class DateInterval implements DtoInterface
{
    public \DateTime $beginAt;
    public \DateTime $endAt;
    public \DateInterval $estimation;
    public bool $default = false;

    public EntityInterface $restaurant;

    public function mutateEstimation(string $estimation): \DateInterval
    {
        return new \DateInterval('PT'.$estimation);
    }

    public function mutateBeginAt(string $input): \DateTime
    {
        if ($dateTime = \DateTime::createFromFormat('!H:i', $input)) {
            return $dateTime;
        }

        if ($dateTime = new \DateTime($input)) {
            return \DateTime::createFromFormat('!H:i', $dateTime->format('H:i'));
        }

        throw (new ConversionException())->setContext(['Unsupported format of date provided' => $input]);
    }

    public function mutateEndAt(string $input): \DateTime
    {
        if ($dateTime = \DateTime::createFromFormat('!H:i', $input)) {
            return $dateTime;
        }

        if ($dateTime = new \DateTime($input)) {
            return \DateTime::createFromFormat('!H:i', $dateTime->format('H:i'));
        }

        throw (new ConversionException())->setContext(['Unsupported format of date provided' => $input]);
    }

    public function toArray(): array
    {
        $result = [];
        foreach (['beginAt', 'endAt', 'estimation', 'restaurant', 'default'] as $key) {
            if (!isset($this->$key)) {
                continue;
            }
            $result[$key] = $this->$key;
        }

        return $result;
    }
}
