<?php
declare(strict_types=1);

namespace App\Dto\Notification;

use App\Dto\DtoInterface;

abstract class AbstractNotification implements DtoInterface
{
    public string $from;
    public array $destinations;
    protected string $template = '';

    public function toArray(): array
    {
        return [
            'from' => $this->from,
            'destinations' => $this->destinations
        ];
    }

    public function getTemplate(): string
    {
        return $this->template;
    }
}
