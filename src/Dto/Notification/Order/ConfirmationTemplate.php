<?php
declare(strict_types=1);

namespace App\Dto\Notification\Order;

use App\Dto\Notification\AbstractNotification;

class ConfirmationTemplate extends AbstractNotification
{
    protected string $template = '{restaurant} will deliver your meal in {time}';
}
