<?php
declare(strict_types=1);

namespace App\Dto\Notification\Order;

use App\Dto\Notification\AbstractNotification;

class ReviewTemplate extends AbstractNotification
{
    protected string $template = '{restaurant} thanks you for your order. Please rate us.';
}
