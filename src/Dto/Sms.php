<?php
declare(strict_types=1);

namespace App\Dto;

class Sms implements DtoInterface
{
    const DEFAULT_ACCEPTABLE_FROM = 'Takeaway';

    public string $from;
    public string $destination;
    public string $message;

    public function toArray(): array
    {
        return [
            'from' => \strlen($this->from) > 10 ? static::DEFAULT_ACCEPTABLE_FROM : $this->from,
            'destination' => $this->destination,
            'message' => $this->message,
        ];
    }
}
