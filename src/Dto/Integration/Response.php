<?php
declare(strict_types=1);

namespace App\Dto\Integration;

use App\Dto\DtoInterface;
use App\Exception\PropertyNotDefinedException;
use App\Exception\PropertyUndefinedException;

class Response implements DtoInterface
{
    public array $headers = [];
    public array $contents = [];

    public function __get(string $key)
    {
        if (!isset($this->contents[$key])) {
            throw (new PropertyUndefinedException())->setContext(['key' => $key]);
        }

        return $this->contents[$key];
    }

    public function getHeaderFirstValue(string $name): string
    {
        $name = \strtolower($name);

        if (!isset($this->headers[$name])) {
            throw (new PropertyUndefinedException())->setContext(['header-name' => $name]);
        }

        return \current($this->headers[$name]);
    }

    public function toArray(): array
    {
        return $this->contents;
    }
}
