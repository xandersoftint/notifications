<?php
declare(strict_types=1);

namespace App\Dto\Integration;

use App\Component\StringAlterationTrait;

class Request
{
    use StringAlterationTrait;

    public const HEADER_API_LANGUAGE = 'Accept-Language';

    public static string $defaultApiLanguage;

    private string $method;
    private string $uri;
    private array $headers;
    private array $content = [];

    private bool $isJson = false;

    public function __construct(string $baseUri)
    {
        $this->uri = $baseUri;
        $this->headers = [
            static::HEADER_API_LANGUAGE => static::$defaultApiLanguage,
        ];
    }

    public function init(string $method, string $uri, array $replacements = []): Request
    {
        $this->method = $method;

        if ($uri) {
            $this->uri = $this->uri . '/' . $this->hydrateUri($uri, $replacements);
        }

        return $this;
    }

    public function getMethod(): string
    {
        return $this->method;
    }

    public function getUri(): string
    {
        return $this->uri;
    }

    public function addHeader(string $key, string $value): Request
    {
        $this->headers[$key] = $value;
        return $this;
    }

    public function setHeaders(array $headers): Request
    {
        $this->headers = $headers;
        return $this;
    }

    public function setContent(array $content): Request
    {
        $this->content = $content;
        return $this;
    }

    public function convertToHttpClientOptions(): array
    {
        $options = [
            'headers' => $this->headers,
        ];

        if ($this->isJson) {
            $options['json'] =  $this->content;
            return $options;
        }

        $options['body'] = $this->content;
        return $options;
    }

    public function toJson(): Request
    {
        $this->isJson = true;
        return $this;
    }
}
