<?php
declare(strict_types=1);

namespace App\Dto\Integration;

use App\Dto\DtoInterface;

class Connection implements DtoInterface
{
    public string $host;
    public string $key;

    public function toArray(): array
    {
        return [
            'host' => $this->host,
            'key' => $this->key,
        ];
    }
}
