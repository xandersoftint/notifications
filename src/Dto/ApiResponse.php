<?php
declare(strict_types=1);

namespace App\Dto;

class ApiResponse implements DtoInterface
{
    public array $messages = [];
    public array $payload = [];
    public int $status;

    public function translate(callable $translator): ApiResponse
    {
        $this->messages = \array_map(
            $translator,
            $this->messages
        );

        return $this;
    }

    public function toArray(): array
    {
        return [
            'messages' => $this->messages,
            'payload' => $this->payload,
        ];
    }
}
