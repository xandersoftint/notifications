<?php
declare(strict_types=1);

namespace App\EventSubscriber;

use App\Component\ApiAwareRequestTrait;
use App\Component\TranslatorAwareTrait;
use App\Dto\ApiResponse;
use App\Exception\ObjectValidationException;
use App\Service\EnvironmentService;
use App\Service\TypeConverterService;
use Psr\Log\LoggerAwareTrait;
use Psr\Log\LoggerInterface;
use Symfony\Component\EventDispatcher\EventSubscriberInterface;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpKernel\Event\ExceptionEvent;
use Symfony\Component\HttpKernel\Event\RequestEvent;
use Symfony\Component\HttpKernel\Event\ResponseEvent;
use Symfony\Component\HttpKernel\KernelEvents;
use Symfony\Contracts\Translation\TranslatorInterface;

class ApiSubscriber implements EventSubscriberInterface
{
    use LoggerAwareTrait, TranslatorAwareTrait, ApiAwareRequestTrait;

    private TypeConverterService $converter;
    private EnvironmentService $environmentService;

    public function __construct(
        LoggerInterface $logger,
        TranslatorInterface $translator,
        TypeConverterService $converter,
        EnvironmentService $environmentService
    ) {
        $this->setLogger($logger);
        $this->setTranslator($translator);
        $this->converter = $converter;
        $this->environmentService = $environmentService;
    }

    public static function getSubscribedEvents()
    {
        return [
            KernelEvents::REQUEST => [
                ['convertContentJsonToArrayFromRequest', 90],
                ['captureLocaleFromApiRequest', 85],
            ],
            KernelEvents::RESPONSE => [
                ['jsonFormatResponse', 0],
            ],
            KernelEvents::EXCEPTION => [
                ['jsonFormatException', -10],
            ]
        ];
    }

    public function convertContentJsonToArrayFromRequest(RequestEvent $event): void
    {
        if (!$event->isMasterRequest() || !$this->isRequestApiCompatible($event->getRequest())) {
            return;
        }

        $request = $event->getRequest();
        $request->initialize(
            $request->query->all(),
            $request->request->all(),
            $request->attributes->all(),
            $request->cookies->all(),
            $request->files->all(),
            $request->server->all(),
            $this->converter->jsonToArray($request->getContent())
        );
    }

    public function jsonFormatResponse(ResponseEvent $event): void
    {
        if (!$event->isMasterRequest() ||
            !$this->isRequestApiCompatible($event->getRequest()) ||
            !\in_array($event->getResponse()->getStatusCode(), [
                Response::HTTP_OK,
                Response::HTTP_PARTIAL_CONTENT
            ])
        ) {
            return;
        }

        $response = new ApiResponse();
        $response->status = $event->getResponse()->getStatusCode();
        $response->payload = $this->converter->jsonToArray($event->getResponse()->getContent());

        $response->translate(function (string $message) {
            return $this->translator->trans($message);
        });

        $event->setResponse(new JsonResponse(
            $this->converter->apiResponseToJson($response),
            $event->getResponse()->getStatusCode(),
            $event->getResponse()->headers->all(),
            true
        ));
    }

    public function jsonFormatException(ExceptionEvent $event): void
    {
        if (!$this->isRequestApiCompatible($event->getRequest())) {
            return;
        }

        $response = $this->hydrateResponseFromThrowable(new ApiResponse(), $event->getThrowable())
            ->translate(
                function (string $message) {
                    return $this->translator->trans($message);
                }
            );

        $event->setResponse(
            new JsonResponse(
                $this->converter->apiResponseToJson($response),
                $event->getThrowable()->getStatusCode(),
                [],
                true
            )
        );
    }

    public function captureLocaleFromApiRequest(RequestEvent $event): void
    {
        $locale = $event->getRequest()->headers->get('Accept-Language');
        if (!$locale) {
            return;
        }

        $this->environmentService->setLocale($locale);
        $event->getRequest()->setLocale($locale);
    }

    private function hydrateResponseFromThrowable(ApiResponse $response, \Throwable $e): ApiResponse
    {
        $response->status = $e->getStatusCode();
        try {
            throw $e->getPrevious();
        } catch (ObjectValidationException $e) {
            foreach ($e->getContext() as $validation) {
                $response->messages[] = $validation->getMessage();
            }
        } catch (\Throwable $t) {
            $response->messages = [$e->getMessage()];
        }

        return $response;
    }
}
