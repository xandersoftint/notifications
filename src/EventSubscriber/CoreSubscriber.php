<?php
declare(strict_types=1);
namespace App\EventSubscriber;

use App\Exception\NotFoundException;
use App\Exception\ObjectValidationException;
use Psr\Log\LoggerAwareTrait;
use Psr\Log\LoggerInterface;
use Symfony\Component\EventDispatcher\EventSubscriberInterface;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpKernel\Event\ExceptionEvent;
use Symfony\Component\HttpKernel\Event\RequestEvent;
use Symfony\Component\HttpKernel\Event\ResponseEvent;
use Symfony\Component\HttpKernel\Exception\HttpException;
use Symfony\Component\HttpKernel\KernelEvents;

class CoreSubscriber implements EventSubscriberInterface
{
    use LoggerAwareTrait;

    public function __construct(LoggerInterface $logger)
    {
        $this->setLogger($logger);
    }

    public static function getSubscribedEvents()
    {
        return [
            KernelEvents::REQUEST => [
                ['logRequest', -100],
            ],
            KernelEvents::RESPONSE => [
                ['logResponse', -100],
            ],
            KernelEvents::EXCEPTION => [
                ['convertNativeExceptionToHttpException', 0],
            ]
        ];
    }

    public function logRequest(RequestEvent $event): void
    {
        if (!$event->isMasterRequest()) {
            return;
        }

        $this->logger->info('Request received: ', [
            'IP' => $event->getRequest()->getClientIp(),
            'URI' => $event->getRequest()->getUri(),
            'Method' => $event->getRequest()->getMethod(),
            'Headers' => $event->getRequest()->headers,
            'Attributes' => $event->getRequest()->attributes,
            'Content' => $event->getRequest()->getContent(),
        ]);
    }

    public function logResponse(ResponseEvent $event): void
    {
        if (!$event->isMasterRequest()) {
            return;
        }

        $this->logger->info('Response sent: ', [
            'Status' => $event->getResponse()->getStatusCode(),
            'Headers' => $event->getResponse()->headers,
            'Content' => $event->getResponse()->getContent(),
        ]);
    }

    public function convertNativeExceptionToHttpException(ExceptionEvent $event): void
    {
        $t = $event->getThrowable();
        $this->logger->error($t);

        if ($t instanceof HttpException) {
            return;
        }

        try {
            throw $t;
        } catch (ObjectValidationException $e) {
            $t = new HttpException(
                Response::HTTP_BAD_REQUEST,
                $e->getMessage(),
                $e
            );
        } catch (NotFoundException $e) {
            $t = new HttpException(
                Response::HTTP_NOT_FOUND,
                $e->getMessage(),
                $e
            );
        } catch (\Throwable $e) {
            $this->logger->alert($e);
            $t = new HttpException(
                Response::HTTP_INTERNAL_SERVER_ERROR,
                'Something went wrong. Please try again later.',
                $e
            );
        }

        $event->setThrowable($t);
    }
}
