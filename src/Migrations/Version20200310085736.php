<?php

declare(strict_types=1);

namespace DoctrineMigrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
final class Version20200310085736 extends AbstractMigration
{
    public function getDescription() : string
    {
        return '';
    }

    public function up(Schema $schema) : void
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() !== 'mysql', 'Migration can only be executed safely on \'mysql\'.');

        $this->addSql('CREATE TABLE sms_transaction (id INT AUTO_INCREMENT NOT NULL, notification_id INT DEFAULT NULL, status INT NOT NULL, response JSON DEFAULT NULL, created_at DATETIME DEFAULT CURRENT_TIMESTAMP NOT NULL, INDEX search_idx (notification_id), PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8mb4 COLLATE `utf8mb4_unicode_ci` ENGINE = InnoDB');
        $this->addSql('CREATE TABLE sms (id INT AUTO_INCREMENT NOT NULL, status VARCHAR(8) NOT NULL, source VARCHAR(32) NOT NULL, destination VARCHAR(16) NOT NULL, message VARCHAR(256) NOT NULL, created_at DATETIME DEFAULT CURRENT_TIMESTAMP NOT NULL, INDEX search_status_idx (status), PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8mb4 COLLATE `utf8mb4_unicode_ci` ENGINE = InnoDB');
        $this->addSql('CREATE TABLE delivery_interval (id INT AUTO_INCREMENT NOT NULL, restaurant_id INT DEFAULT NULL, estimation VARCHAR(255) NOT NULL COMMENT \'(DC2Type:dateinterval)\', is_default TINYINT(1) DEFAULT \'0\' NOT NULL, begin_at DATETIME DEFAULT NULL, end_at DATETIME DEFAULT NULL, created_at DATETIME DEFAULT CURRENT_TIMESTAMP NOT NULL, updated_at DATETIME NOT NULL on update CURRENT_TIMESTAMP DEFAULT CURRENT_TIMESTAMP, INDEX search_idx (restaurant_id), PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8mb4 COLLATE `utf8mb4_unicode_ci` ENGINE = InnoDB');
        $this->addSql('CREATE TABLE restaurant (id INT AUTO_INCREMENT NOT NULL, code VARCHAR(32) NOT NULL, name VARCHAR(64) NOT NULL, created_at DATETIME DEFAULT CURRENT_TIMESTAMP NOT NULL, updated_at DATETIME NOT NULL on update CURRENT_TIMESTAMP DEFAULT CURRENT_TIMESTAMP, UNIQUE INDEX UNIQ_EB95123F77153098 (code), INDEX search_idx (code), PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8mb4 COLLATE `utf8mb4_unicode_ci` ENGINE = InnoDB');
        $this->addSql('ALTER TABLE sms_transaction ADD CONSTRAINT FK_ABB42D4AEF1A9D84 FOREIGN KEY (notification_id) REFERENCES sms (id)');
        $this->addSql('ALTER TABLE delivery_interval ADD CONSTRAINT FK_78A70EA7B1E7706E FOREIGN KEY (restaurant_id) REFERENCES restaurant (id)');
    }

    public function down(Schema $schema) : void
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() !== 'mysql', 'Migration can only be executed safely on \'mysql\'.');

        $this->addSql('ALTER TABLE sms_transaction DROP FOREIGN KEY FK_ABB42D4AEF1A9D84');
        $this->addSql('ALTER TABLE delivery_interval DROP FOREIGN KEY FK_78A70EA7B1E7706E');
        $this->addSql('DROP TABLE sms_transaction');
        $this->addSql('DROP TABLE sms');
        $this->addSql('DROP TABLE delivery_interval');
        $this->addSql('DROP TABLE restaurant');
    }
}
