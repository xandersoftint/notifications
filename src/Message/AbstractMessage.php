<?php
declare(strict_types=1);

namespace App\Message;

abstract class AbstractMessage
{
    public const TOPIC = 'generic';
}
