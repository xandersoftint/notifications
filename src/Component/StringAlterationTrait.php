<?php
declare(strict_types=1);

namespace App\Component;

trait StringAlterationTrait
{
    public function hydrateUri(string $uri, array $replacements = []): string
    {
        return \str_replace(
            \array_map(
                function (string $value) {
                    return '{' . $value . '}';
                },
                \array_keys($replacements)
            ),
            \array_values($replacements),
            $uri
        );
    }

    public function toReadableDateInterval(\DateInterval $interval, callable $translate = null): string
    {
        $format = '';
        if ($interval->d) {
            $format .= '%d days ';
        }

        if ($interval->h) {
            $format .= '%h hours ';
        }

        if ($interval->i) {
            $format .= '%i minutes ';
        }

        if ($interval->s) {
            $format .= '%s seconds ';
        }

        if (null !== $translate) {
            $format = $translate($format);
        }

        return $interval->format(\trim($format));
    }
}
