<?php
declare(strict_types=1);
namespace App\Component;

trait TransactionAwareTrait
{
    protected AbstractCRUDRepository $repository;

    public function setRepository(AbstractCRUDRepository $repository): void
    {
        $this->repository = $repository;
    }

    public function beginTransaction(): void
    {
        $this->repository->getTransactionLock();
    }

    public function releaseTransaction(): void
    {
        $this->repository->releaseTransactionLock();
    }

    public function revertTransaction(): void
    {
        $this->repository->revertTransaction();
    }

    public function encapsulateTransaction(callable $function)
    {
        $this->beginTransaction();

        try {
            $returnValue = $function();
        } catch (\Throwable $e) {
            $this->revertTransaction();
            throw $e;
        }

        $this->releaseTransaction();
        return $returnValue;
    }
}
