<?php
declare(strict_types=1);

namespace App\Component;

use App\Exception\TypeUnsupportedException;

abstract class AbstractCollection extends \ArrayObject
{
    final public function __construct($input = array(), $flags = 0, $iterator_class = "ArrayIterator")
    {
        foreach ($input as $value) {
            if (!$this->isTypeSupported($value)) {
                throw (new TypeUnsupportedException())->setContext([
                    'given_type' => \get_class($value),
                    'trigger' => static::class,
                ]);
            }
        }
        parent::__construct($input, $flags, $iterator_class);
    }

    final public function offsetSet($index, $newval)
    {
        if (!$this->isTypeSupported($newval)) {
            throw (new TypeUnsupportedException())->setContext([
                'given_type' => \get_class($newval),
                'trigger' => static::class,
            ]);
        }

        parent::offsetSet($index, $newval);
    }

    final public function exchangeArray($input)
    {
        foreach ($input as $value) {
            if (!$this->isTypeSupported($value)) {
                throw (new TypeUnsupportedException())->setContext([
                    'given_type' => \get_class($value),
                    'trigger' => static::class,
                ]);
            }
        }

        parent::exchangeArray($input);
    }

    abstract protected function isTypeSupported($value): bool;
}
