<?php
declare(strict_types=1);
namespace App\Component;

use App\Exception\ObjectValidationException;
use Symfony\Component\Validator\Validator\ValidatorInterface;

trait ValidatorAwareTrait
{
    protected ValidatorInterface $validator;

    public function setValidator(ValidatorInterface $validator): void
    {
        $this->validator = $validator;
    }

    protected function validate($object, $groups = null): void
    {
        $errors = $this->validator->validate($object, null, $groups);
        if (!$errors->count()) {
            return;
        }

        throw (new ObjectValidationException())->setContext($errors);
    }
}
