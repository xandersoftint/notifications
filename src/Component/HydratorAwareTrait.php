<?php
declare(strict_types=1);

namespace App\Component;

use App\Service\ObjectHydratorService;

trait HydratorAwareTrait
{
    private ObjectHydratorService $hydrator;

    public function setHydrator(ObjectHydratorService $hydrator): void
    {
        $this->hydrator = $hydrator;
    }
}
