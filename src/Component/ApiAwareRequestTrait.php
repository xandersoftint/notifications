<?php
declare(strict_types=1);

namespace App\Component;

use Symfony\Component\HttpFoundation\Request;

trait ApiAwareRequestTrait
{
    private static string $CONTENT_TYPE_JSON  = 'json';

    private function isRequestApiCompatible(Request $request): bool
    {
        return self::$CONTENT_TYPE_JSON === $request->getContentType();
    }
}
