<?php
declare(strict_types=1);
namespace App\Component;

use App\Entity\EntityInterface;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\ORM\QueryBuilder;
use Doctrine\ORM\Tools\Pagination\Paginator;

abstract class AbstractCRUDRepository extends ServiceEntityRepository
{
    private static bool $inLock = false;

    public function getTransactionLock(): AbstractCRUDRepository
    {
        if ($this->getEntityManager()->getConnection()->isTransactionActive()) {
            return $this;
        }

        self::$inLock = true;
        $this->getEntityManager()->beginTransaction();
        return $this;
    }

    public function releaseTransactionLock(): AbstractCRUDRepository
    {
        if (!$this->getEntityManager()->getConnection()->isTransactionActive()) {
            return $this;
        }

        self::$inLock = false;
        $this->getEntityManager()->flush();
        $this->getEntityManager()->commit();
        return $this;
    }

    public function revertTransaction(): AbstractCRUDRepository
    {
        if (!$this->getEntityManager()->getConnection()->isTransactionActive()) {
            return $this;
        }

        self::$inLock = false;
        $this->getEntityManager()->rollback();
        return $this;
    }

    public function save(EntityInterface $entity): EntityInterface
    {
        $this->getEntityManager()->persist($entity);
        if (self::$inLock) {
            return $entity;
        }

        $this->getEntityManager()->flush();
        return $entity;
    }

    public function update(): AbstractCRUDRepository
    {
        if (self::$inLock) {
            return $this;
        }

        $this->getEntityManager()->flush();
        return $this;
    }

    public function remove(EntityInterface $entity): void
    {
        $this->getEntityManager()->remove($entity);
        if (self::$inLock) {
            return;
        }

        $this->getEntityManager()->flush();
    }

    public function getQueryCount(QueryBuilder $query): int
    {
        return \count(new Paginator($query, true));
    }
}
