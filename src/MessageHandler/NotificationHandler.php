<?php
declare(strict_types=1);

namespace App\MessageHandler;

use App\Entity\NotificationInterface;
use App\Facade\NotificationFacadeInterface;
use App\Message\AbstractMessage;

class NotificationHandler extends AbstractHandler
{
    private NotificationFacadeInterface $notification;

    public function __construct(
        NotificationFacadeInterface $notification
    ) {
        $this->notification = $notification;
    }

    protected function process(AbstractMessage $message): void
    {
        /** @var NotificationInterface $notification */
        $notification = $this->notification->get($message->getId());
        $this->notification->send($notification);
    }
}
