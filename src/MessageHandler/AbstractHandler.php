<?php
declare(strict_types=1);

namespace App\MessageHandler;

use App\Message\AbstractMessage;
use App\Service\TypeConverterService;
use Psr\Log\LoggerAwareTrait;
use Psr\Log\LoggerInterface;
use Symfony\Component\Messenger\Handler\MessageHandlerInterface;

abstract class AbstractHandler implements MessageHandlerInterface
{
    use LoggerAwareTrait;

    private TypeConverterService $converter;

    /** @required */
    public function setLogger(LoggerInterface $logger): void
    {
        $this->logger = $logger;
    }

    /** @required */
    public function setConverter(TypeConverterService $converter): void
    {
        $this->converter = $converter;
    }

    public function __invoke(AbstractMessage $message)
    {
        $this->logger->debug("Received Message", [
            'handler' => static::class,
            'message' => $this->converter->objectToArray($message),
            'timestamp' => \time(),
        ]);
        $this->process($message);
        $this->logger->debug("Finished Message", [
            'handler' => static::class,
            'timestamp' => \time(),
        ]);
    }

    abstract protected function process(AbstractMessage $message): void;
}
