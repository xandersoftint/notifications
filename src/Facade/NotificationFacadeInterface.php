<?php
declare(strict_types=1);

namespace App\Facade;

use App\Dto\DtoInterface;
use App\Entity\NotificationInterface;

interface NotificationFacadeInterface
{
    public function create(DtoInterface $dto): NotificationInterface;
    public function envelopeAndSend(string $from, array $destinations, string $payload, int $delayedSeconds = 0): void;
    public function send(NotificationInterface $sms): NotificationInterface;
    public function generate(array $data): DtoInterface;
}
