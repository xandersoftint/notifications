<?php
declare(strict_types=1);

namespace App\Facade;

use App\Dto\DtoInterface;
use App\Dto\Restaurant as RestaurantDto;
use App\Dto\DateInterval as DateIntervalDto;
use App\Entity\Restaurant;
use App\Service\DeliveryIntervalService;
use App\Service\RestaurantService;

class RestaurantFacade extends AbstractFacade
{
    private DeliveryIntervalService $deliveryIntervalService;

    public function __construct(
        RestaurantService $restaurantService,
        DeliveryIntervalService $deliveryIntervalService
    ) {
        $this->deliveryIntervalService = $deliveryIntervalService;

        $this->setMainService($restaurantService);
    }

    public function create(RestaurantDto $restaurantDto): Restaurant
    {
        return $this->service->encapsulateTransaction(function () use ($restaurantDto) {
            $restaurant = $this->service->create($restaurantDto->toArray());
            /** @var DtoInterface $intervalDto */
            foreach ($restaurantDto->intervals as $intervalDto) {
                $this->deliveryIntervalService->create(
                    \array_merge([
                        'restaurant' => $restaurant,
                    ], $intervalDto->toArray())
                );
            }

            return $restaurant;
        });
    }

    public function generate(array $data): DtoInterface
    {
        $restaurantDto = $this->converter->arrayToDto($data, RestaurantDto::class);
        $restaurantDto->intervals = \array_map(function (array $intervalData) {
            return $this->converter->arrayToDto($intervalData, DateIntervalDto::class);
        }, $data['intervals'] ?? []);

        return $restaurantDto;
    }

    public function getEstimationForCurrentTime(Restaurant $restaurant): \DateInterval
    {
        $deliveryInterval = $this->deliveryIntervalService->getByRestaurantAndTime(
            $restaurant,
            \DateTime::createFromFormat('!H:i', (new \DateTime())->format('H:i'))
        );

        return $deliveryInterval->getEstimation();
    }
}
