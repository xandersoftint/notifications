<?php
declare(strict_types=1);

namespace App\Facade;

use App\Dto\DtoInterface;
use App\Entity\NotificationInterface;
use App\Dto\Sms as SmsDto;
use App\Exception\CoreException;
use App\Exception\IntegrationRespondedException;
use App\Integration\NotificationInterface as IntegrationNotificationInterface;
use App\Message\SmsMessage;
use App\MessageProducer\NotificationProducer;
use App\Service\SmsService;
use App\Service\SmsTransactionService;

class SmsFacade extends AbstractFacade implements NotificationFacadeInterface
{
    private SmsTransactionService $transactionService;
    private IntegrationNotificationInterface $integration;
    private NotificationProducer $producer;

    public function __construct(
        SmsService $smsService,
        SmsTransactionService $transactionService,
        IntegrationNotificationInterface $integration,
        NotificationProducer $producer
    ) {
        $this->transactionService = $transactionService;
        $this->integration = $integration;
        $this->producer = $producer;

        $this->setMainService($smsService);
    }

    public function create(DtoInterface $smsDto): NotificationInterface
    {
        return $this->service->create($smsDto->toArray());
    }

    public function envelopeAndSend(string $from, array $destinations, string $message, int $delayedSeconds = 0): void
    {
        foreach ($destinations as $destination) {
            try {
                $sms = $this->create($this->generate([
                    'from' => $from,
                    'destination' => $destination,
                    'message' => $message,
                ]));
                $this->publishMessengerNotification($sms->getId(), $delayedSeconds);
            } catch (CoreException $e) {
                $this->logger->warning($e, $e->getContext());
                continue;
            }
        }
    }

    public function send(NotificationInterface $sms): NotificationInterface
    {
        try {
            $response = $this->integration->send([$sms->getDestination()], $sms->getFrom(), $sms->getMessage());
            $transactions = function () use ($response, $sms) {
                $sms = $this->service->update($sms, [
                    'status' => NotificationInterface::STATUS_SENT,
                ]);

                $this->transactionService->create([
                    'sms' => $sms,
                    'status' => 200,
                    'response' => $response->toArray()
                ]);

                return $sms;
            };
        } catch (IntegrationRespondedException $e) {
            $transactions = function () use ($e, $sms) {
                $sms = $this->service->update($sms, [
                    'status' => NotificationInterface::STATUS_ERROR,
                ]);

                $this->transactionService->create([
                    'sms' => $sms,
                    'status' => $e->getCode(),
                    'response' => $e->getResponse()->toArray()
                ]);

                return $sms;
            };
        }

        return $this->service->encapsulateTransaction($transactions);
    }

    public function generate(array $data): DtoInterface
    {
        return $this->converter->arrayToDto($data, SmsDto::class);
    }

    private function publishMessengerNotification(int $id, int $delayedSeconds): void
    {
        $messageClass = $this->producer->getMessageClassByTopic(SmsMessage::TOPIC);
        if (!$delayedSeconds) {
            $this->producer->publish(new $messageClass($id));
            return;
        }

        $this->producer->publishForDelayedProcessing(new $messageClass($id), $delayedSeconds);
    }
}
