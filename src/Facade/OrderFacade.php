<?php
declare(strict_types=1);

namespace App\Facade;

use App\Component\StringAlterationTrait;
use App\Component\TranslatorAwareTrait;
use App\Dto\Notification\AbstractNotification;
use App\Dto\Notification\Order\ConfirmationTemplate as ConfirmationNotification;
use App\Dto\Notification\Order\ReviewTemplate as ReviewNotification;
use App\Service\EnvironmentService;
use Symfony\Contracts\Translation\TranslatorInterface;

class OrderFacade extends AbstractFacade
{
    use StringAlterationTrait, TranslatorAwareTrait;

    private RestaurantFacade $restaurantFacade;
    private NotificationFacadeInterface $notificationFacade;
    private EnvironmentService $environment;

    public function __construct(
        RestaurantFacade $restaurantFacade,
        NotificationFacadeInterface $notificationFacade,
        TranslatorInterface $translator,
        EnvironmentService $environment
    ) {
        $this->restaurantFacade = $restaurantFacade;
        $this->notificationFacade = $notificationFacade;
        $this->environment = $environment;

        $this->setTranslator($translator);
    }

    public function sendNotification(AbstractNotification $notification, callable $delayComputeMiddleware = null): void
    {
        $delay = 0;
        $restaurant = $this->restaurantFacade->getByCode($notification->from);
        $estimation = $this->restaurantFacade->getEstimationForCurrentTime($restaurant);

        if ($delayComputeMiddleware) {
            $delay = $delayComputeMiddleware($estimation);
        }

        $replacements = [
            'restaurant' => $restaurant->getName(),
            'time' => $this->toReadableDateInterval(
                $estimation,
                function (string $id) {
                    return $this->translator->trans($id);
                }
            ),
        ];

        $message = $this->hydrateUri(
            $this->translator->trans($notification->getTemplate()),
            $replacements
        );

        $this->notificationFacade->envelopeAndSend(
            $restaurant->getName(),
            $notification->destinations,
            $message,
            $delay
        );
    }

    public function sendOrderNotification(ConfirmationNotification $notification)
    {
        $this->sendNotification($notification);
        $this->sendNotification(
            $this->generateReviewNotification($notification->toArray()),
            function (\DateInterval $estimation): int {
                $delay = $this->environment->getConfigDelayOrderReviewNotificationSeconds();
                $delay += $estimation->h * 3600 + $estimation->i * 60 + $estimation->s;
                return $delay;
            }
        );
    }

    public function generateConfirmationNotification(array $data): AbstractNotification
    {
        return $this->converter->arrayToDto($data, ConfirmationNotification::class);
    }

    public function generateReviewNotification(array $data): AbstractNotification
    {
        return $this->converter->arrayToDto($data, ReviewNotification::class);
    }
}
