<?php
declare(strict_types=1);

namespace App\Facade;

use App\Service\AbstractTransactionAwareService;
use App\Service\TypeConverterService;
use Psr\Log\LoggerAwareTrait;
use Psr\Log\LoggerInterface;

abstract class AbstractFacade
{
    use LoggerAwareTrait;

    protected AbstractTransactionAwareService $service;
    protected TypeConverterService $converter;

    /** @required */
    public function setLogger(LoggerInterface $logger): void
    {
        $this->logger = $logger;
    }

    /** @required */
    public function setConverter(TypeConverterService $service): void
    {
        $this->converter = $service;
    }

    public function setMainService(AbstractTransactionAwareService $service): void
    {
        $this->service = $service;
    }

    public function __call($name, $arguments)
    {
        return $this->service->$name(...$arguments);
    }
}
