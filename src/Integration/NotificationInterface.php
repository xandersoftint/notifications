<?php
declare(strict_types=1);

namespace App\Integration;

use App\Dto\Integration\Response as IntegrationResponse;

interface NotificationInterface
{
    public function send(array $recipients, string $sender, string $body): IntegrationResponse;
}
