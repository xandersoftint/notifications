<?php
declare(strict_types=1);

namespace App\Integration;

use App\Service\EnvironmentService;
use App\Service\IntegrationService;

use App\Dto\Integration\Request as IntegrationRequest;
use App\Dto\Integration\Response as IntegrationResponse;
use Symfony\Component\HttpFoundation\Request;

class MessageBirdNotification implements NotificationInterface
{
    public const API_SEND = 'messages/';

    private IntegrationService $integration;
    private EnvironmentService $environment;

    public function __construct(EnvironmentService $environment, IntegrationService $integration)
    {
        $this->environment = $environment;
        $this->integration = $integration;
    }

    public function send(array $recipients, string $sender, string $body): IntegrationResponse
    {
        $request = new IntegrationRequest($this->environment->getSms()->host);
        $request->init(Request::METHOD_POST, static::API_SEND)
            ->addHeader('Authorization', "AccessKey {$this->environment->getSms()->key}");

        $request->setContent([
            'recipients' => $recipients,
            'originator' => $sender,
            'body' => $body,
        ]);

        return $this->integration->execute($request);
    }
}
