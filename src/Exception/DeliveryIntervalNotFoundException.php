<?php
declare(strict_types=1);

namespace App\Exception;

class DeliveryIntervalNotFoundException extends NotFoundException
{
    protected $message = 'Searched delivery interval was not found.';
}
