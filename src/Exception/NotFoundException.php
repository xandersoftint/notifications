<?php
declare(strict_types=1);

namespace App\Exception;

class NotFoundException extends CoreException
{
    protected $message = 'An object was not found';
}
