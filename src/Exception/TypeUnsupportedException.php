<?php
declare(strict_types=1);

namespace App\Exception;

class TypeUnsupportedException extends CoreException
{
    protected $message = "Object of given type is not allowed here";
}
