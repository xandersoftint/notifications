<?php
declare(strict_types=1);

namespace App\Exception;

class RestaurantNotFoundException extends NotFoundException
{
    protected $message = "Searched restaurant was not found";
}
