<?php
declare(strict_types=1);

namespace App\Exception;

class PropertyUndefinedException extends CoreException
{
    protected $message = 'Property is undefined. Please set it first.';
}
