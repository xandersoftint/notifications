<?php
declare(strict_types=1);

namespace App\Exception;

class IntegrationException extends CoreException
{
    protected $message = "Cannot establish an integration connection";
}
