<?php
declare(strict_types=1);

namespace App\Exception;

use App\Dto\Integration\Response;

class IntegrationRespondedException extends IntegrationException
{
    private Response $response;
    protected $message = "Connection has been established but the request was invalid";

    public function setResponse(Response $content): IntegrationRespondedException
    {
        $this->response = $content;
        return $this;
    }

    public function getResponse(): Response
    {
        return $this->response;
    }
}
