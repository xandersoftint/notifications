<?php
declare(strict_types=1);

namespace App\Exception;

class CoreException extends \Exception
{
    protected $message = 'Something went wrong. Please try again later.';

    protected $context = [];

    public function setContext($context): CoreException
    {
        $this->context = $context;
        return $this;
    }

    public function getContext()
    {
        return $this->context;
    }
}
