<?php
declare(strict_types=1);

namespace App\MessageProducer;

use App\Exception\CoreException;
use App\Message\AbstractMessage;
use App\Message\SmsMessage;
use Symfony\Component\Messenger\MessageBusInterface;
use Symfony\Component\Messenger\Stamp\DelayStamp;
use Symfony\Component\Messenger\Transport\AmqpExt\AmqpStamp;

class NotificationProducer
{
    const PROCESS_DELAY_90_MINUTES = 90 * 60;

    public const MESSAGES = [
        SmsMessage::TOPIC => SmsMessage::class,
    ];

    private MessageBusInterface $bus;

    public function __construct(MessageBusInterface $bus)
    {
        $this->bus = $bus;
    }

    public function publish(AbstractMessage $message): void
    {
        $this->bus->dispatch($message, [
            new AmqpStamp($message::TOPIC, AMQP_NOPARAM)
        ]);
    }

    public function publishForDelayedProcessing(AbstractMessage $message, int $seconds): void
    {
        $this->bus->dispatch($message, [
            new DelayStamp($seconds * 1000),
            new AmqpStamp($message::TOPIC, AMQP_NOPARAM),
        ]);
    }

    public function getMessageClassByTopic(string $topic): string
    {
        if (!isset(static::MESSAGES[$topic])) {
            throw (new CoreException())->setContext(['notification-topic' => $topic]);
        }

        return static::MESSAGES[$topic];
    }
}
