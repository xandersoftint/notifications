<?php
declare(strict_types=1);

namespace App\Repository;

use App\Component\AbstractCRUDRepository;
use App\Entity\EntityCollection;
use App\Entity\PaginatedCollection;
use App\Entity\SmsTransaction;
use Doctrine\Persistence\ManagerRegistry;
use Symfony\Component\HttpFoundation\Response;

class SmsTransactionRepository extends AbstractCRUDRepository
{
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, SmsTransaction::class);
    }

    public function listErrorsBy(\DateTime $lastDays): PaginatedCollection
    {
        $qb = $this->createQueryBuilder('st');
        $qb = $qb->join('st.sms', 'sms', 'WITH', 'sms.id = st.sms', 'sms.id');
        $qb->where('st.status != :successStatus')
            ->andWhere('st.createdAt >= :startAt')
            ->setParameter('successStatus', Response::HTTP_OK)
            ->setParameter('startAt', $lastDays);

        $collection = $qb->getQuery()->getResult();
        return new PaginatedCollection(
            new EntityCollection($collection),
            $this->count([]),
            $this->getQueryCount($qb)
        );
    }

    public function listBy(\DateTime $lastDays, int $offset, int $limit, string $search = ''): PaginatedCollection
    {
        $qb = $this->createQueryBuilder('st');
        $qb->select('st')
            ->addSelect(
            'CASE WHEN st.status != :successStatus '.
                'THEN CASE WHEN st.createdAt > :startAt '.
                'THEN 0 ELSE -1 END '.
            'ELSE 1 END as order_filter'
        )->join('st.sms', 'sms', 'WITH', 'sms.id = st.sms', 'sms.id')
            ->andHaving('order_filter != -1')
            ->orderBy('order_filter', 'ASC')
            ->addOrderBy('st.createdAt', 'DESC')
            ->setParameter('successStatus', Response::HTTP_OK)
            ->setParameter('startAt', $lastDays);

        if ($search) {
            $qb->andWhere(
                $qb->expr()->orX(
                    $qb->expr()->like('st.status', ':search'),
                    $qb->expr()->like('sms.message', ':search'),
                    $qb->expr()->like('sms.from', ':search'),
                    $qb->expr()->like('sms.destination', ':search'),
                )
            )->setParameter('search', '%' . $search . '%');
        }

        $filteredCount = $this->getQueryCount($qb);
        $collection = $qb->setFirstResult($offset)
            ->setMaxResults($limit)
            ->getQuery()
            ->getResult();

        return new PaginatedCollection(
            new EntityCollection(
                \array_map(function (array $selection) {
                    return \current($selection);
                }, $collection)),
            $this->count([]),
            $filteredCount
        );
    }
}
