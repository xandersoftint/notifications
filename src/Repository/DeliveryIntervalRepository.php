<?php
declare(strict_types=1);

namespace App\Repository;

use App\Component\AbstractCRUDRepository;
use App\Entity\DeliveryInterval;
use App\Entity\DeliveryIntervalCollection;
use App\Entity\EntityInterface;
use App\Entity\PaginatedCollection;
use App\Entity\Restaurant;
use App\Exception\DeliveryIntervalNotFoundException;
use Doctrine\ORM\NoResultException;
use Doctrine\Persistence\ManagerRegistry;

class DeliveryIntervalRepository extends AbstractCRUDRepository
{
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, DeliveryInterval::class);
    }

    public function find($id, $lockMode = null, $lockVersion = null): EntityInterface
    {
        $entity = parent::find($id, $lockMode, $lockVersion);
        if (null === $entity) {
            throw (new DeliveryIntervalNotFoundException())->setContext(['id' => $id]);
        }

        return $entity;
    }

    public function findByRestaurant(Restaurant $restaurant): PaginatedCollection
    {
        $criteria = [
            'restaurant' => $restaurant
        ];

        $collection = parent::findBy($criteria);
        return new PaginatedCollection(
            new DeliveryIntervalCollection($collection),
            $this->count([]),
            $this->count($criteria)
        );
    }

    public function findByRestaurantAndTime(Restaurant $restaurant, \DateTime $time): EntityInterface
    {
        $qb = $this->createQueryBuilder('di');
        $qb->where('di.restaurant = :restaurant')
            ->andWhere('di.beginAt <= :interval')
            ->andWhere('di.endAt >= :interval')
            ->setParameter('restaurant', $restaurant)
            ->setParameter('interval', $time)
            ->orderBy('di.id', 'DESC');

        try {
            $entity = $qb->getQuery()->getSingleResult();
        } catch (NoResultException $e) {
            throw (new DeliveryIntervalNotFoundException())
                ->setContext(['restaurant' => $restaurant->getId(), 'time' => $time]);
        }

        return $entity;
    }

    public function findDefaultByRestaurant(Restaurant $restaurant): EntityInterface
    {
        $entity = parent::findOneBy([
            'restaurant' => $restaurant,
            'default' => true,
        ]);

        if (null === $entity) {
            throw (new DeliveryIntervalNotFoundException())
                ->setContext(['restaurant' => $restaurant->getId(), 'default' => true]);
        }

        return $entity;
    }
}
