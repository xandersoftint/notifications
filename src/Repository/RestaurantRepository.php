<?php
declare(strict_types=1);

namespace App\Repository;

use App\Component\AbstractCRUDRepository;
use App\Entity\EntityInterface;
use App\Entity\Restaurant;
use App\Exception\RestaurantNotFoundException;
use Doctrine\Persistence\ManagerRegistry;

class RestaurantRepository extends AbstractCRUDRepository
{
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, Restaurant::class);
    }

    public function find($id, $lockMode = null, $lockVersion = null): EntityInterface
    {
        $entity = parent::find($id, $lockMode, $lockVersion);
        if (null === $entity) {
            throw (new RestaurantNotFoundException())->setContext(['id' => $id]);
        }

        return $entity;
    }

    public function findByCode(string $code): EntityInterface
    {
        $entity = parent::findOneBy(['code' => $code]);
        if (null === $entity) {
            throw (new RestaurantNotFoundException())->setContext(['code' => $code]);
        }

        return $entity;
    }
}
