<?php
declare(strict_types=1);

namespace App\Repository;

use App\Component\AbstractCRUDRepository;
use App\Entity\EntityInterface;
use App\Entity\Sms;
use App\Exception\SmsNotFoundException;
use Doctrine\Persistence\ManagerRegistry;

class SmsRepository extends AbstractCRUDRepository
{
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, Sms::class);
    }

    public function find($id, $lockMode = null, $lockVersion = null): EntityInterface
    {
        $entity = parent::find($id, $lockMode, $lockVersion);
        if (null === $entity) {
            throw (new SmsNotFoundException())->setContext(['id' => $id]);
        }

        return $entity;
    }
}
